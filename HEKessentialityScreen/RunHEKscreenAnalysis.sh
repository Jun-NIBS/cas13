#!/bin/bash
#$ -cwd

SCRIPTS="./scripts/"
FILES="./data/"

# You may need to have R3.5.1 
# You may need to install some R packages

module purge
module load R/3.5.1

# Runs all data processing starting from raw counts until the end
# data tables will be saved in ./data
# A set of figures will be saved in ./figures
# figures used for the manuscript are: CRISPRscores_vs_D0.pdf, CRISPRscores_ByGeneByGuide.pdf
# This script will not check for pre-existing data, but overwrite all files 

Rscript ./scripts/ProcessData_HEKessentiality.R

rm Rplots.pdf

exit