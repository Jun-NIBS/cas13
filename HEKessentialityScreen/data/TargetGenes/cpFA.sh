#!/bin/bash
#$ -cwd

while read -r line; do
	dir=$(echo $line | awk -F" " '{print substr($1,1,10)}')
	cp ../../../TranscriptomicPredictions_GENCODEv19/${dir}/${line}.fasta ./
done < $1
