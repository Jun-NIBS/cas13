#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

library(leaps)
library(BMA)
library(MASS)
library(glmnet, lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(pls, lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(caret)
library(stabs,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(mboost,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(modeltools)
#devtools::install_version("earth",version = "4.6.1", repos = "http://cran.us.r-project.org",lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(TeachingDemos,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(plotmo,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(earth,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(randomForest,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(cowsay,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(e1071)
library(LiblineaR,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")
library(Metrics,lib.loc = "~/R/x86_64-redhat-linux-gnu-library/3.4")

filenum = args[1] # filenum=1


########### subroutines 
preprocessdata <- function(data){
  data = data[complete.cases(data),fields]
  
 cols= c( "X27_A" , "X27_C" , "X27_G" , "X27_T" , "X26_A" , "X26_C" , "X26_G" , "X26_T" ,
          "X25_A" , "X25_C" , "X25_G" , "X25_T" , "X24_A" , "X24_C" , "X24_G" , "X24_T" ,     
  "X23_A" , "X23_C" , "X23_G" , "X23_T" , "X22_A" , "X22_C" , "X22_G" , "X22_T" , "X21_A" , "X21_C" , "X21_G"  ,    
   "X21_T" , "X20_A" , "X20_C" , "X20_G" , "X20_T" , "X19_A" , "X19_C" , "X19_G" , "X19_T" , "X18_A" , "X18_C"  ,    
   "X18_G" , "X18_T" , "X17_A" , "X17_C" , "X17_G" , "X17_T" , "X16_A" , "X16_C" , "X16_G" , "X16_T" , "X15_A"  ,    
   "X15_C" , "X15_G" , "X15_T" , "X14_A" , "X14_C" , "X14_G" , "X14_T" , "X13_A" , "X13_C" , "X13_G" , "X13_T"  ,    
   "X12_A" , "X12_C" , "X12_G" , "X12_T" , "X11_A" , "X11_C" , "X11_G" , "X11_T" , "X10_A" , "X10_C" , "X10_G"  ,    
   "X10_T" , "X9_A" , "X9_C" , "X9_G" , "X9_T" , "X8_A" , "X8_C" , "X8_G" , "X8_T" , "X7_A" , "X7_C" ,      
   "X7_G" , "X7_T" , "X6_A" , "X6_C" , "X6_G" , "X6_T" , "X5_A" , "X5_C" , "X5_G" , "X5_T" , "X4_A" ,      
   "X4_C" , "X4_G" , "X4_T" , "X3_A" , "X3_C" , "X3_G" , "X3_T" , "X2_A" , "X2_C" , "X2_G" , "X2_T" ,      
   "X1_A" , "X1_C" , "X1_G" , "X1_T")
 

  cols.GC = cols[grep("G|C", cols)]
 
  data = cbind(data , rowSums(data[,cols.GC])/(length(cols)/4))
  colnames(data)[ncol(data)] <- "GCcontent"
  
  return(data)
}
GetFetures = function(x){strsplit(x, split = "\\+")[[1]]}




########### load data 
load(paste("ModelData-", filenum, ".RData", sep=""))
fields = c(colnames(train)[-c(2,3,4)])

grouped.formula = formula(meanCS.BIN1 ~  X.4_A + X.4_C + X.4_G + X.4_T + X.3_A + X.3_C + X.3_G + X.3_T + X.2_A + X.2_C      
                           + X.2_G + X.2_T + X.1_A + X.1_C + X.1_G + X.1_T + X27_A + X27_C + X27_G + X27_T + X26_A      
                          + X26_C + X26_G + X26_T + X25_A + X25_C + X25_G + X25_T + X24_A + X24_C + X24_G + X24_T      
                          + X23_A + X23_C + X23_G + X23_T + X22_A + X22_C + X22_G + X22_T + X21_A + X21_C + X21_G      
                          + X21_T + X20_A + X20_C + X20_G + X20_T + X19_A + X19_C + X19_G + X19_T + X18_A + X18_C      
                          + X18_G + X18_T + X17_A + X17_C + X17_G + X17_T + X16_A + X16_C + X16_G + X16_T + X15_A      
                          + X15_C + X15_G + X15_T + X14_A + X14_C + X14_G + X14_T + X13_A + X13_C + X13_G + X13_T      
                          + X12_A + X12_C + X12_G + X12_T + X11_A + X11_C + X11_G + X11_T + X10_A + X10_C + X10_G      
                          + X10_T + X9_A + X9_C + X9_G + X9_T + X8_A + X8_C + X8_G + X8_T + X7_A + X7_C       
                          + X7_G + X7_T + X6_A + X6_C + X6_G + X6_T + X5_A + X5_C + X5_G + X5_T + X4_A       
                          + X4_C + X4_G + X4_T + X3_A + X3_C + X3_G + X3_T + X2_A + X2_C + X2_G + X2_T       
                          + X1_A + X1_C + X1_G + X1_T + X.1_A.1 + X.1_C.1 + X.1_G.1 + X.1_T.1 + X.2_A.1 + X.2_C.1 + X.2_G.1    
                          + X.2_T.1 + X.3_A.1 + X.3_C.1 + X.3_G.1 + X.3_T.1 + X.4_A.1 + X.4_C.1 + X.4_G.1 + X.4_T.1 
                          + X.3_AA + X.3_AC     
                           + X.3_AG + X.3_AT + X.3_CA + X.3_CC + X.3_CG + X.3_CT + X.3_GA + X.3_GC + X.3_GG + X.3_GT + X.3_TA     
                           + X.3_TC + X.3_TG + X.3_TT + X.2_AA + X.2_AC + X.2_AG + X.2_AT + X.2_CA + X.2_CC + X.2_CG + X.2_CT     
                           + X.2_GA + X.2_GC + X.2_GG + X.2_GT + X.2_TA + X.2_TC + X.2_TG + X.2_TT + X.1_AA + X.1_AC + X.1_AG     
                           + X.1_AT + X.1_CA + X.1_CC + X.1_CG + X.1_CT + X.1_GA + X.1_GC + X.1_GG + X.1_GT + X.1_TA + X.1_TC     
                           + X.1_TG + X.1_TT + X27_AA + X27_AC + X27_AG + X27_AT + X27_CA + X27_CC + X27_CG + X27_CT + X27_GA     
                           + X27_GC + X27_GG + X27_GT + X27_TA + X27_TC + X27_TG + X27_TT + X26_AA + X26_AC + X26_AG + X26_AT     
                           + X26_CA + X26_CC + X26_CG + X26_CT + X26_GA + X26_GC + X26_GG + X26_GT + X26_TA + X26_TC + X26_TG     
                           + X26_TT + X25_AA + X25_AC + X25_AG + X25_AT + X25_CA + X25_CC + X25_CG + X25_CT + X25_GA + X25_GC     
                           + X25_GG + X25_GT + X25_TA + X25_TC + X25_TG + X25_TT + X24_AA + X24_AC + X24_AG + X24_AT + X24_CA     
                           + X24_CC + X24_CG + X24_CT + X24_GA + X24_GC + X24_GG + X24_GT + X24_TA + X24_TC + X24_TG + X24_TT     
                           + X23_AA + X23_AC + X23_AG + X23_AT + X23_CA + X23_CC + X23_CG + X23_CT + X23_GA + X23_GC + X23_GG     
                           + X23_GT + X23_TA + X23_TC + X23_TG + X23_TT + X22_AA + X22_AC + X22_AG + X22_AT + X22_CA + X22_CC
                           + X22_CG + X22_CT + X22_GA + X22_GC + X22_GG + X22_GT + X22_TA + X22_TC + X22_TG + X22_TT + X21_AA     
                           + X21_AC + X21_AG + X21_AT + X21_CA + X21_CC + X21_CG + X21_CT + X21_GA + X21_GC + X21_GG + X21_GT     
                           + X21_TA + X21_TC + X21_TG + X21_TT + X20_AA + X20_AC + X20_AG + X20_AT + X20_CA + X20_CC + X20_CG     
                           + X20_CT + X20_GA + X20_GC + X20_GG + X20_GT + X20_TA + X20_TC + X20_TG + X20_TT + X19_AA + X19_AC     
                           + X19_AG + X19_AT + X19_CA + X19_CC + X19_CG + X19_CT + X19_GA + X19_GC + X19_GG + X19_GT + X19_TA     
                           + X19_TC + X19_TG + X19_TT + X18_AA + X18_AC + X18_AG + X18_AT + X18_CA + X18_CC + X18_CG + X18_CT     
                           + X18_GA + X18_GC + X18_GG + X18_GT + X18_TA + X18_TC + X18_TG + X18_TT + X17_AA + X17_AC + X17_AG     
                           + X17_AT + X17_CA + X17_CC + X17_CG + X17_CT + X17_GA + X17_GC + X17_GG + X17_GT + X17_TA + X17_TC     
                           + X17_TG + X17_TT + X16_AA + X16_AC + X16_AG + X16_AT + X16_CA + X16_CC + X16_CG + X16_CT + X16_GA     
                           + X16_GC + X16_GG + X16_GT + X16_TA + X16_TC + X16_TG + X16_TT + X15_AA + X15_AC + X15_AG + X15_AT     
                           + X15_CA + X15_CC + X15_CG + X15_CT + X15_GA + X15_GC + X15_GG + X15_GT + X15_TA + X15_TC + X15_TG     
                           + X15_TT + X14_AA + X14_AC + X14_AG + X14_AT + X14_CA + X14_CC + X14_CG + X14_CT + X14_GA + X14_GC     
                           + X14_GG + X14_GT + X14_TA + X14_TC + X14_TG + X14_TT + X13_AA + X13_AC + X13_AG + X13_AT + X13_CA     
                           + X13_CC + X13_CG + X13_CT + X13_GA + X13_GC + X13_GG + X13_GT + X13_TA + X13_TC + X13_TG + X13_TT     
                           + X12_AA + X12_AC + X12_AG + X12_AT + X12_CA + X12_CC + X12_CG + X12_CT + X12_GA + X12_GC + X12_GG     
                           + X12_GT + X12_TA + X12_TC + X12_TG + X12_TT + X11_AA + X11_AC + X11_AG + X11_AT + X11_CA + X11_CC     
                           + X11_CG + X11_CT + X11_GA + X11_GC + X11_GG + X11_GT + X11_TA + X11_TC + X11_TG + X11_TT + X10_AA     
                           + X10_AC + X10_AG + X10_AT + X10_CA + X10_CC + X10_CG + X10_CT + X10_GA + X10_GC + X10_GG + X10_GT     
                           + X10_TA + X10_TC + X10_TG + X10_TT + X9_AA + X9_AC + X9_AG + X9_AT + X9_CA + X9_CC + X9_CG      
                           + X9_CT + X9_GA + X9_GC + X9_GG + X9_GT + X9_TA + X9_TC + X9_TG + X9_TT + X8_AA + X8_AC      
                           + X8_AG + X8_AT + X8_CA + X8_CC + X8_CG + X8_CT + X8_GA + X8_GC + X8_GG + X8_GT + X8_TA      
                           + X8_TC + X8_TG + X8_TT + X7_AA + X7_AC + X7_AG + X7_AT + X7_CA + X7_CC + X7_CG + X7_CT      
                           + X7_GA + X7_GC + X7_GG + X7_GT + X7_TA + X7_TC + X7_TG + X7_TT + X6_AA + X6_AC + X6_AG      
                           + X6_AT + X6_CA + X6_CC + X6_CG + X6_CT + X6_GA + X6_GC + X6_GG + X6_GT + X6_TA + X6_TC     
                           + X6_TG + X6_TT + X5_AA + X5_AC + X5_AG + X5_AT + X5_CA + X5_CC + X5_CG + X5_CT + X5_GA      
                           + X5_GC + X5_GG + X5_GT + X5_TA + X5_TC + X5_TG + X5_TT + X4_AA + X4_AC + X4_AG + X4_AT      
                           + X4_CA + X4_CC + X4_CG + X4_CT + X4_GA + X4_GC + X4_GG + X4_GT + X4_TA + X4_TC + X4_TG      
                           + X4_TT + X3_AA + X3_AC + X3_AG + X3_AT + X3_CA + X3_CC + X3_CG + X3_CT + X3_GA + X3_GC      
                           + X3_GG + X3_GT + X3_TA + X3_TC + X3_TG + X3_TT + X2_AA + X2_AC + X2_AG + X2_AT + X2_CA      
                           + X2_CC + X2_CG + X2_CT + X2_GA + X2_GC + X2_GG + X2_GT + X2_TA + X2_TC + X2_TG + X2_TT      
                           + X1_AA + X1_AC + X1_AG + X1_AT + X1_CA + X1_CC + X1_CG + X1_CT + X1_GA + X1_GC + X1_GG      
                           + X1_GT + X1_TA + X1_TC + X1_TG + X1_TT + X.1_AA.1 + X.1_AC.1 + X.1_AG.1 + X.1_AT.1 + X.1_CA.1 + X.1_CC.1   
                           + X.1_CG.1 + X.1_CT.1 + X.1_GA.1 + X.1_GC.1 + X.1_GG.1 + X.1_GT.1 + X.1_TA.1 + X.1_TC.1 + X.1_TG.1 + X.1_TT.1 + X.2_AA.1   
                           + X.2_AC.1 + X.2_AG.1 + X.2_AT.1 + X.2_CA.1 + X.2_CC.1 + X.2_CG.1 + X.2_CT.1 + X.2_GA.1 + X.2_GC.1 + X.2_GG.1 + X.2_GT.1   
                           + X.2_TA.1 + X.2_TC.1 + X.2_TG.1 + X.2_TT.1 + X.3_AA.1 + X.3_AC.1 + X.3_AG.1 + X.3_AT.1 + X.3_CA.1 + X.3_CC.1 + X.3_CG.1  
                           + X.3_CT.1 + X.3_GA.1 + X.3_GC.1 + X.3_GG.1 + X.3_GT.1 + X.3_TA.1 + X.3_TC.1 + X.3_TG.1 + X.3_TT.1 + X.4_AA + X.4_AC     
                           + X.4_AG + X.4_AT + X.4_CA + X.4_CC + X.4_CG + X.4_CT + X.4_GA + X.4_GC + X.4_GG + X.4_GT + X.4_TA     
                          + X.4_TC + X.4_TG + X.4_TT + GCcontent)




########### initialize output file

Model.rho <- vector()
Model.sp <- vector()
FeatureSelection <- matrix( 0 , nrow = 11 , ncol = length(fields)+1)
colnames(FeatureSelection) = c(fields,"GCcontent")
rownames(FeatureSelection) = c("fwd-ind", "StepBIC", "StepAIC","randomForest.IncMSE","randomForest.IncNodePurity","lambda.1se","lambda.min","SVM.lin", "SVM.poly","SVM.rad","SVM.sigm")




########### Process train/test data
train = preprocessdata(train)
test = preprocessdata(test)



site.binary.all = c()
lm.pred = c()
pred.context.plus = c()
pred.context.only = c()
step.pred = c()
glmnet.pred = c()
bicreg.pred = c()
mars.pred = c()


site.train = train
site.test = test

train.matrix = model.matrix(grouped.formula, site.train)
test.matrix = model.matrix(grouped.formula, site.test)

dependencies = findLinearCombos(train.matrix) #uses QR decomposition to find linear dependencies in matrix (caret package)
train.matrix = train.matrix[, -c(1)] #dependencies$remove,
test.matrix = test.matrix[, -c(1)] #dependencies$remove,
 
 numeric = which(colnames(site.train) == "GCcontent" )
 tmpmean = quantile(site.train[,numeric], 0.05)
 tmpsd = quantile(site.train[,numeric], 0.95)-quantile(site.train[,numeric], 0.05)
 site.train[,numeric] = scale(site.train[,numeric], center=tmpmean, scale=tmpsd) # scale both sets in terms of training set points
 site.test[,numeric] = scale(site.test[,numeric], center=tmpmean, scale=tmpsd)
 
 site.train[,numeric][site.train[,numeric] > 1] = 1
 site.train[,numeric][site.train[,numeric] < 0] = 0
 site.test[,numeric][site.test[,numeric] > 1] = 1
 site.test[,numeric][site.test[,numeric] < 0] = 0

 numeric = apply(train.matrix, 2, function(x){length(unique(x))>2})
 tmpmean = quantile(train.matrix[,numeric], 0.05)
 tmpsd = quantile(train.matrix[,numeric], 0.95)-quantile(train.matrix[,numeric], 0.05)
 train.matrix[,numeric] = scale(train.matrix[,numeric], center=tmpmean, scale=tmpsd) # scale both sets in terms of training set points
 test.matrix[,numeric] = scale(test.matrix[,numeric], center=tmpmean, scale=tmpsd)
 
 train.matrix[,numeric][train.matrix[,numeric] > 1] = 1
 train.matrix[,numeric][train.matrix[,numeric] < 0] = 0
 test.matrix[,numeric][test.matrix[,numeric] > 1] = 1
 test.matrix[,numeric][test.matrix[,numeric] < 0] = 0



 ######################  Linear Models
 
 #say("GCcontent_only")
 lm.context.only = lm(meanCS.BIN1 ~ GCcontent , data = site.train)
 pred.context.only = c(pred.context.only, predict(lm.context.only, newdata = site.test))
 #say(cor(site.test$meanCS.BIN1, predict(lm.context.only, newdata = site.test), method="pearson"))
 Model.rho[1] <-cor(site.test$meanCS.BIN1, predict(lm.context.only, newdata = site.test), method="pearson")
 Model.sp[1]  <-cor(site.test$meanCS.BIN1, predict(lm.context.only, newdata = site.test), method="spearman")
 names(Model.rho)[1] <- "GCcontent"


########### Models



# #say("SVM tune")
# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$meanCS.BIN1 ,ranges = list(epsilon = seq(0,1,0.5), cost = 2^(2:4)), kernel = "linear") # from e1071 package
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# FeatureSelection["SVM.lin",colnames(W)] <- W[1,]
# pred <- predict(best_mod, test.matrix)
# #rmse(pred,site.test$meanCS.BIN1)
# Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.lin"

# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$meanCS.BIN1 ,ranges =  list(epsilon = seq(0,1,0.5), cost = 2^(2:4)), kernel = "polynomial") # from e1071 package
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# FeatureSelection["SVM.poly",colnames(W)] <- W[1,]
# pred <- predict(best_mod, test.matrix)
# Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.poly"

svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$meanCS.BIN1 ,ranges =  list(epsilon = seq(0,1,0.5), cost = 2^(2:4)), kernel = "radial") # from e1071 package
best_mod <- svm_tune$best.model
W = t(best_mod$coefs) %*% best_mod$SV
FeatureSelection["SVM.rad",colnames(W)] <- W[1,]
pred <- predict(best_mod, test.matrix)
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, pred, method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, pred, method="spearman")
names(Model.rho)[length(Model.rho)] <- "SVM+tuning.radbias"

# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$meanCS.BIN1 ,ranges = list(epsilon = seq(0,1,0.5), cost = 2^(2:4)), kernel = "sigmoid") # from e1071 package
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# FeatureSelection["SVM.sigm",colnames(W)] <- W[1,]
# pred <- predict(best_mod, test.matrix)
# Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.sigm"

# say("fwd-indiv regsubsets")
sets.fit=regsubsets(grouped.formula, data = site.train, nvmax=15, nbest=1, method="forward", really.big=T) #from leaps package -- nvmax is max subset size // forward or backward or exhaustive
bestfeatures = (rank(summary(sets.fit)$bic)==1) #get index of model with minimum BIC
coefs = coef(sets.fit, which(bestfeatures)) #get coefficients of best model
indices = summary(sets.fit)$which[bestfeatures][-1] #get indices of chosen features of best model
tmptest = cbind(rep(1,nrow(test.matrix)), test.matrix[,indices])
vals = t(coefs %*% t(tmptest))
best.formula = paste(colnames(summary(sets.fit)$outmat)[indices], collapse="+") #extract model with minimum BIC
#say(cor(site.test$meanCS.BIN1, vals, method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, vals, method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, vals, method="spearman")
names(Model.rho)[length(Model.rho)] <- "regsubsets fwd-indiv"

#say("stepBIC")
stepbic.fit = stepAIC(lm(meanCS.BIN1 ~ 1, data = site.train), scope = list( upper = grouped.formula, lower = ~1),
                      direction="both", k = log(nrow(site.train)), trace = 0) #k=logN for BIC/ = 2 for AIC, k = log(nrow(site.train)) from MASS package
step.pred = c(step.pred, predict(stepbic.fit, newdata = site.test))
# say(as.character(cor(site.test$meanCS.BIN1, predict(stepbic.fit, newdata = site.test), method="pearson")))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, predict(stepbic.fit, newdata = site.test), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, predict(stepbic.fit, newdata = site.test), method="spearman")  
names(Model.rho)[length(Model.rho)] <- "stepBIC"

# say("stepAIC")
step.fit = stepAIC(lm(meanCS.BIN1 ~ 1, data = site.train), scope = list( upper = grouped.formula, lower = ~1),
                   direction="both", k = 2, trace = 0) #k=logN for BIC/ = 2 for AIC, k = log(nrow(site.train)) from MASS package
step.pred = c(step.pred, predict(step.fit, newdata = site.test))
# say(as.character(cor(site.test$meanCS.BIN1, predict(step.fit, newdata = site.test), method="pearson")))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, predict(step.fit, newdata = site.test), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, predict(step.fit, newdata = site.test), method="spearman")
names(Model.rho)[length(Model.rho)] <- "stepAIC"

# say("Lasso")
#fit = glmnet(train.matrix, site.train$meanCS.BIN1, family="gaussian", alpha = 1)
#plot(fit, label = TRUE)
#coef(fit,s=0.05)
glmnet.fit = cv.glmnet(train.matrix, site.train$meanCS.BIN1, family="gaussian", nfolds=10, alpha = 1) # from glmnet package
lse = coef(glmnet.fit, s = "lambda.1se")[,1]
lmin = coef(glmnet.fit, s = "lambda.min")[,1]
FeatureSelection["lambda.1se",names(lse)[which(lse != 0 & names(lse) != "(Intercept)" )]] <- 1
FeatureSelection["lambda.min",names(lmin)[which(lmin != 0 & names(lmin) != "(Intercept)" )]] <- 1
# say(as.character(cor(site.test$meanCS.BIN1, predict(glmnet.fit, type="link", newx=test.matrix), method="pearson")))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, predict(glmnet.fit, type="link", newx=test.matrix), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, predict(glmnet.fit, type="link", newx=test.matrix), method="spearman")
names(Model.rho)[length(Model.rho)] <- "Lasso"

# ay("MARS")
mars.fit = earth(grouped.formula, data = site.train, degree = 1, trace = 0, nk = 500)  # from earth package
# say(cor(site.test$meanCS.BIN1, predict(mars.fit, newdata = site.test), method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, predict(mars.fit, newdata = site.test), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, predict(mars.fit, newdata = site.test), method="spearman")
names(Model.rho)[length(Model.rho)] <- "MARS"

# say("RandomForest")
rf.fit = randomForest(train.matrix, site.train$meanCS.BIN1, importance = T)  # from randomForest package
#varImp(rf.fit)
# say(cor(site.test$meanCS.BIN1, predict(rf.fit, newdata = test.matrix), method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, predict(rf.fit, newdata = test.matrix), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, predict(rf.fit, newdata = test.matrix), method="spearman")
imp = importance(rf.fit)
FeatureSelection["randomForest.IncMSE",rownames(imp)] <- imp[,1]
FeatureSelection["randomForest.IncNodePurity",rownames(imp)] <- imp[,2]
names(Model.rho)[length(Model.rho)] <- "RandomForest"

# say("PCA")
pcr.fit = pcr(grouped.formula, data = site.train)  # from pls package
pcr.pred = predict(pcr.fit, ncomp = 5, newdata = site.test)
# say(cor(site.test$meanCS.BIN1, pcr.pred, method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$meanCS.BIN1, pcr.pred, method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$meanCS.BIN1, pcr.pred, method="spearman")
names(Model.rho)[length(Model.rho)] <- "PCA"

#say("PLSR")
plsr.fit = plsr(grouped.formula, data = site.train) # from pls package
plsr.pred = predict(plsr.fit, ncomp = 5, newdata = site.test)
#say(cor(site.test$meanCS.BIN1, plsr.pred, method="pearson"))
Model.rho[length(Model.rho)+1] <-cor(site.test$meanCS.BIN1, plsr.pred, method="pearson")
Model.sp[length(Model.sp)+1] <-cor(site.test$meanCS.BIN1, plsr.pred, method="spearman")
names(Model.rho)[length(Model.rho)] <- "PLSR"


names(Model.sp) = names(Model.rho)



FeatureSelection["fwd-ind",GetFetures(best.formula)] <- 1
FeatureSelection["StepBIC",gsub("\n","",gsub(" ","",GetFetures(as.character(formula(stepbic.fit)[3]))))] <- 1
FeatureSelection["StepAIC",gsub("\n","",gsub(" ","",GetFetures(as.character(formula(step.fit)[3]))))] <- 1

write.table(FeatureSelection , file = paste0("FeatureSelection_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
write.table(t(as.data.frame(Model.rho)) , file = paste0("Model.rho_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
write.table(t(as.data.frame(Model.sp)) , file = paste0("Model.spearman_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
