#!/usr/bin/env Rscript
# load libraries
suppressWarnings(suppressMessages(library(reshape2, quietly = T)))
suppressWarnings(suppressMessages(library(ggplot2, quietly = T)))


# load data
Full = read.csv("Learning_approaches_Spearman_v1.csv")
Minimal = read.csv("Learning_approaches_Spearman_v2.csv")
DoenchRegression = read.csv("Learning_approaches_Spearman_v3.csv")

# get Random Forest
GetMethod = function(x,col = "RandomForest"){x[,col]}

RF = list()
RF[[1]] = GetMethod(Full)
RF[[2]] = GetMethod(Minimal)
RF[[3]] = GetMethod(DoenchRegression, col = "SVM.tuning" )
names(RF) = c("Random Forest full", "Random Forest minimal", "SVM + L1 Doench Regression")

df = melt(RF)
colnames(df) = c("Spearman", "Model")
df$Model = factor(df$Model , levels = names(RF) )

medians = aggregate(Spearman ~  Model, df, median)

pdf("Model_comparison.pdf", width = 4, height = 3, useDingbats = F)
ggplot(data = df, aes( x = Model , y = Spearman)) +
  geom_boxplot( outlier.size = 0.1,  aes(fill = Model)) +
  scale_fill_manual( values = c( "#ef3b2c","#cb181d","#3182bd")) +
  theme_classic() +
  ylab("spearmean coef. to held-out data") +
  xlab("") +
  #ylim(c(-0.12,0.72))+
  #theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  coord_fixed(ratio=20) + 
  geom_text(data = medians, aes(label = round(Spearman,2), y = Spearman + 0.025), colour = "white", size=2.5) + 
  theme(axis.title.x=element_blank(),axis.text.x=element_blank(),axis.ticks.x = element_blank(), axis.line.x = element_blank())
dev.off()
