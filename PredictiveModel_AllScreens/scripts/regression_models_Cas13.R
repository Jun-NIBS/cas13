#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(leaps, quietly = T))
suppressMessages(library(BMA, quietly = T))
suppressMessages(library(MASS, quietly = T))
suppressMessages(library(glmnet, quietly = T))
suppressMessages(library(pls, quietly = T))
suppressMessages(library(caret, quietly = T))
suppressMessages(library(stabs, quietly = T))
suppressMessages(library(mboost, quietly = T))
suppressMessages(library(modeltools, quietly = T))
suppressMessages(library(earth, quietly = T))
suppressMessages(library(randomForest, quietly = T))
suppressMessages(library(LiblineaR, quietly = T))
suppressMessages(library(e1071, quietly = T))
suppressMessages(library(Metrics, quietly = T))
suppressMessages(library(cowsay, quietly = T))

filenum = args[1] # filenum=1
Version = as.character(args[2]) #  Version = "v3"
Formulas = as.character(args[3]) #  Formulas = "./data/formulas.txt"






###### sub-routines #################################

preprocessdata <- function(data){
  data = data[complete.cases(data),fields]
  return(data)
}
GetFetures = function(x){strsplit(x, split = "\\+")[[1]]}


########################## sub-routines end  ########

###### execute ######################################



########### load data 

#train/test sets
load(paste("./tmp_RData_",Version,"/ModelData-", filenum, ".RData", sep=""))

# formula to test along with respective fields
FORMULAS = read.delim( file = Formulas , header = F , sep = "\t" , stringsAsFactors = F )
idx = grep( paste0(Version,"$") , FORMULAS$V1)
feats = gsub(" ","",gsub("\n","",strsplit( x = FORMULAS$V3[idx] , split = ",")[[1]]))
fields = c( FORMULAS$V2[idx] , feats)

grouped.formula = as.formula( paste0(c(fields[1] , "~" , paste0(fields[-1] , collapse = " + ")), collapse = " "))





########### initialize output file

Model.rho <- vector()
Model.sp <- vector()
FeatureSelection <- matrix( 0 , nrow = 11 , ncol = length(fields))
colnames(FeatureSelection) = fields
rownames(FeatureSelection) = c("fwd-ind", "StepBIC", "StepAIC","randomForest.IncMSE","randomForest.IncNodePurity","lambda.1se","lambda.min","SVM.lin", "SVM.poly","SVM.rad","SVM.default")




########### Process train/test data
train = preprocessdata(train)
test = preprocessdata(test)



site.binary.all = c()
lm.pred = c()
pred.context.plus = c()
pred.context.only = c()
step.pred = c()
glmnet.pred = c()
bicreg.pred = c()
mars.pred = c()


site.train = train
site.test = test

train.matrix = model.matrix(grouped.formula, site.train)
test.matrix = model.matrix(grouped.formula, site.test)

dependencies = findLinearCombos(train.matrix) #uses QR decomposition to find linear dependencies in matrix (caret package)
if(length(dependencies$remove) > 0){
  
  del = colnames(train.matrix)[dependencies$remove]
  
  train.matrix = train.matrix[, -c( which( colnames(train.matrix) %in% del), 1)]
  test.matrix = test.matrix[, -c( which( colnames(test.matrix) %in% del), 1)]
  
  site.train = site.train[, -c( which( colnames(site.train) %in% del))]
  site.test = site.test[, -c( which( colnames(site.test) %in% del))]
  
  
  # rm = which(colnames(FeatureSelection) %in% del)
  # if(length(rm)>0){
  #   FeatureSelection = FeatureSelection[ , -rm ]
  # }
  
  rm = which( fields %in% del)
  if(length(rm) > 0){
    fields = fields[-rm]
  }

  f = as.character(grouped.formula)
  tmp = gsub(" ","",gsub("\n","",strsplit(f[3], split = "\\+")[[1]]))
  rm = which( tmp %in% del)
  if(length(rm) > 0){
    tmp = tmp[-rm]
  }
  f[3] = paste0(tmp , collapse = "+")
  grouped.formula = as.formula( paste0(c(f[2] , f[1] , f[3]), collapse = " "))
  
  #train.matrix = model.matrix(grouped.formula, site.train)[, -c(1)]
  #test.matrix = model.matrix(grouped.formula, site.test)[, -c(1)]
  
  
}else{
  train.matrix = train.matrix[, -c(1)]
  test.matrix = test.matrix[, -c(1)]
}




numeric = sapply(site.train, is.numeric) & colnames(site.train) != "value"  & colnames(site.train) != "Gquad"  & colnames(site.train) != "DR" & grepl("[A,C,G,T]_",colnames(site.train)) == FALSE #DO NOT SCALE response value to [0,1] interval
tmpmean = apply(site.train[,numeric], 2, function(x) quantile(x, 0.05))
tmpsd = apply(site.train[,numeric], 2, function(x) quantile(x, 0.95)-quantile(x, 0.05))

site.train[,numeric] = scale(site.train[,numeric], center=tmpmean, scale=tmpsd) # scale both sets in terms of training set points
site.test[,numeric] = scale(site.test[,numeric], center=tmpmean, scale=tmpsd)

site.train[,numeric][site.train[,numeric] > 1] = 1
site.train[,numeric][site.train[,numeric] < 0] = 0
site.test[,numeric][site.test[,numeric] > 1] = 1
site.test[,numeric][site.test[,numeric] < 0] = 0

numeric = apply(train.matrix, 2, function(x){length(unique(x))>2})
tmpmean = apply(train.matrix[,numeric], 2, function(x) quantile(x, 0.05))
tmpsd = apply(train.matrix[,numeric], 2, function(x) quantile(x, 0.95)-quantile(x, 0.05))
train.matrix[,numeric] = scale(train.matrix[,numeric], center=tmpmean, scale=tmpsd) # scale both sets in terms of training set points
test.matrix[,numeric] = scale(test.matrix[,numeric], center=tmpmean, scale=tmpsd)

train.matrix[,numeric][train.matrix[,numeric] > 1] = 1
train.matrix[,numeric][train.matrix[,numeric] < 0] = 0
test.matrix[,numeric][test.matrix[,numeric] > 1] = 1
test.matrix[,numeric][test.matrix[,numeric] < 0] = 0

######################  Linear Models

#lm.context.only = lm(value ~ NTdens_max_A + NTdens_max_C + NTdens_max_G + NTdens_max_T + NTdens_max_AT  +  NTdens_max_GC , data = site.train)
#pred.context.only = c(pred.context.only, predict(lm.context.only, newdata = site.test))
#say(cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson"))
#Model.rho[length(Model.rho)+1] <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson")
#Model.sp[length(Model.sp)+1]  <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="spearman")
#names(Model.rho)[length(Model.rho)] <- "Context.pos"

#lm.context.only = lm(value ~ NTdens_min_A  +   NTdens_min_C  +   NTdens_min_G  +   NTdens_min_T  +   NTdens_min_AT  +  NTdens_min_GC , data = site.train)
#pred.context.only = c(pred.context.only, predict(lm.context.only, newdata = site.test))
#say(cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson"))
#Model.rho[length(Model.rho)+1] <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson")
#Model.sp[length(Model.sp)+1]  <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="spearman")
#names(Model.rho)[length(Model.rho)] <- "Context.neg"

#say("NTcontext")
#lm.context.only = lm(value ~ NTdens_max_A + NTdens_max_C + NTdens_max_G + NTdens_max_T + NTdens_max_AT  +  NTdens_max_GC + 
#                       NTdens_min_A  +   NTdens_min_C  +   NTdens_min_G  +   NTdens_min_T   , data = site.train)
#pred.context.only = c(pred.context.only, predict(lm.context.only, newdata = site.test))
#say(cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson"))
#Model.rho[length(Model.rho)+1] <- cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson")
#Model.sp[length(Model.sp)+1]  <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="spearman")
#names(Model.rho)[length(Model.rho)] <- "NTcontext"

#say("NTcontextPlus")
#lm.context.only = lm(value ~ MFE + DR + Gquad + Log10_Unpaired + hybMFE_3.12 + hybMFE_15.9  + 
#                       NTdens_max_A + NTdens_max_C + NTdens_max_G + NTdens_max_T + NTdens_max_AT  +  NTdens_max_GC + 
#                       NTdens_min_A  +   NTdens_min_C  +   NTdens_min_G  +   NTdens_min_T  , data = site.train)
#pred.context.only = c(pred.context.only, predict(lm.context.only, newdata = site.test))
#say(cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson"))
#Model.rho[length(Model.rho)+1] <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="pearson")
#Model.sp[length(Model.sp)+1]  <-cor(site.test$value, predict(lm.context.only, newdata = site.test), method="spearman")
#names(Model.rho)[length(Model.rho)] <- "NTcontextPlus"



######################  Learning precedures


# # say("SVM tune linear")
# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$value ,ranges = list(epsilon = seq(0,1,0.025), cost = 2^(0:8)), kernel = "linear") # from e1071 package
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# FeatureSelection["SVM.lin",colnames(W)] <- W[1,]
# pred <- predict(best_mod, test.matrix)
# #rmse(pred,site.test$value)
# Model.rho[length(Model.rho)+1] <- cor(site.test$value, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$value, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.lin"

# # say("SVM tune polynomial")
# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$value ,ranges = list(epsilon = seq(0,1,0.025), cost = 2^(0:8)), kernel = "polynomial") # from e1071 package
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# FeatureSelection["SVM.poly",colnames(W)] <- W[1,]
# pred <- predict(best_mod, test.matrix)
# Model.rho[length(Model.rho)+1] <- cor(site.test$value, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$value, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.poly"

# say("SVM tune radial" )
# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$value ,ranges = list(epsilon = seq(0,1,0.025), cost = 2^(0:8)), kernel = "radial" , scale = FALSE) # from e1071 package #seq(0,1,0.025), cost = 2^(0:8)
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# weights = W[1, grep("value|ntercep",colnames(W) , invert = T)]
# FeatureSelection["SVM.rad",names(weights)] <- weights
# pred <- predict(best_mod, test.matrix)
# Model.rho[length(Model.rho)+1] <- cor(site.test$value, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$value, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.radbias"
# tune = cbind.data.frame( best_mod$epsilon, best_mod$cost, best_mod$gamma )
# rownames(tune) = filenum
# colnames(tune) = c("epsilon","cost","gamma")


say("SVM default")
svm.fit = svm( x = train.matrix , site.train$value , scale = F , kernel = "radial"  )
W <- t(svm.fit$coefs) %*% svm.fit$SV
weights = W[1, grep("value|ntercep",colnames(W) , invert = T)]
FeatureSelection["SVM.default",names(weights)] <- weights
pred <- predict(svm.fit, test.matrix)
Model.rho[length(Model.rho)+1] <- cor(site.test$value, pred, method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, pred, method="spearman")
names(Model.rho)[length(Model.rho)] <- "SVM.default"


# say("SVM tune sigmoid")
# svm_tune = tune(method = svm , train.x = train.matrix, train.y = site.train$value ,ranges = list(epsilon = seq(0,1,0.025), cost = 2^(0:8)), kernel = "sigmoid") # from e1071 package
# best_mod <- svm_tune$best.model
# W = t(best_mod$coefs) %*% best_mod$SV
# FeatureSelection["SVM.sigm",colnames(W)] <- W[1,]
# pred <- predict(best_mod, test.matrix)
# Model.rho[length(Model.rho)+1] <- cor(site.test$value, pred, method="pearson")
# Model.sp[length(Model.sp)+1] <- cor(site.test$value, pred, method="spearman")
# names(Model.rho)[length(Model.rho)] <- "SVM+tuning.sigm"


say("fwd-indiv regsubsets")
sets.fit=regsubsets(grouped.formula, data = site.train, nvmax=15, nbest=1, method="forward", really.big=T) #from leaps package -- nvmax is max subset size // forward or backward or exhaustive
bestfeatures = (rank(summary(sets.fit)$bic)==1) #get index of model with minimum BIC
coefs = coef(sets.fit, which(bestfeatures)) #get coefficients of best model
s = summary(sets.fit)$which
indices = colnames(s)[s[bestfeatures,]] #get indices of chosen features of best model
indices = indices[grep( "Intercept" , indices , invert = T)]
tmptest = cbind(rep(1,nrow(test.matrix)), test.matrix[,indices])
# indices = summary(sets.fit)$which[bestfeatures] #get indices of chosen features of best model
# tmptest = cbind(rep(1,nrow(test.matrix)), test.matrix[,indices])
vals = t(coefs %*% t(tmptest))
best.formula = paste(indices, collapse="+") #extract model with minimum BIC
#say(cor(site.test$value, vals, method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, vals, method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, vals, method="spearman")
names(Model.rho)[length(Model.rho)] <- "regsubsetss fwd-indiv"



say("stepBIC")
stepbic.fit = stepAIC(lm(value ~ 1, data = site.train), scope = list( upper = grouped.formula, lower = ~1),
                      direction="both", k = log(nrow(site.train)), trace = 0) #k=logN for BIC/ = 2 for AIC, k = log(nrow(site.train)) from MASS package
step.pred = c(step.pred, predict(stepbic.fit, newdata = site.test))
# say(as.character(cor(site.test$value, predict(stepbic.fit, newdata = site.test), method="pearson")))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, predict(stepbic.fit, newdata = site.test), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, predict(stepbic.fit, newdata = site.test), method="spearman")  
names(Model.rho)[length(Model.rho)] <- "stepBIC"

say("stepAIC")
step.fit = stepAIC(lm(value ~ 1, data = site.train), scope = list( upper = grouped.formula, lower = ~1),
                   direction="both", k = 2, trace = 0) #k=logN for BIC/ = 2 for AIC, k = log(nrow(site.train)) from MASS package
step.pred = c(step.pred, predict(step.fit, newdata = site.test))
# say(as.character(cor(site.test$value, predict(step.fit, newdata = site.test), method="pearson")))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, predict(step.fit, newdata = site.test), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, predict(step.fit, newdata = site.test), method="spearman")
names(Model.rho)[length(Model.rho)] <- "stepAIC"

say("Lasso")
glmnet.fit = cv.glmnet(train.matrix, site.train$value, family="gaussian", nfolds=10, alpha = 1) # from glmnet package
lse = coef(glmnet.fit, s = "lambda.1se")[,1]
lmin = coef(glmnet.fit, s = "lambda.min")[,1]
FeatureSelection["lambda.1se",names(lse)[which(lse != 0 & names(lse) != "(Intercept)" )]] <- 1
FeatureSelection["lambda.min",names(lmin)[which(lmin != 0 & names(lmin) != "(Intercept)" )]] <- 1
# say(as.character(cor(site.test$value, predict(glmnet.fit, type="link", newx=test.matrix), method="pearson")))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, predict(glmnet.fit, type="link", newx=test.matrix), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, predict(glmnet.fit, type="link", newx=test.matrix), method="spearman")
names(Model.rho)[length(Model.rho)] <- "Lasso"

say("MARS")
mars.fit = earth(grouped.formula, data = site.train, degree = 1, trace = 0, nk = 500) # from earth package
# say(cor(site.test$value, predict(mars.fit, newdata = site.test), method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, predict(mars.fit, newdata = site.test), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, predict(mars.fit, newdata = site.test), method="spearman")
names(Model.rho)[length(Model.rho)] <- "MARS"


say("RandomForest")
rf.fit = randomForest(train.matrix, site.train$value, importance = T) # from randomForest package
#varImp(rf.fit)
# say(cor(site.test$value, predict(rf.fit, newdata = test.matrix), method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, predict(rf.fit, newdata = test.matrix), method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, predict(rf.fit, newdata = test.matrix), method="spearman")
imp = importance(rf.fit)
FeatureSelection["randomForest.IncMSE",rownames(imp)] <- imp[,1]
FeatureSelection["randomForest.IncNodePurity",rownames(imp)] <- imp[,2]
names(Model.rho)[length(Model.rho)] <- "RandomForest"



say("PCA")
pcr.fit = pcr(grouped.formula, data = site.train) # from pls package
pcr.pred = predict(pcr.fit, ncomp = 5, newdata = site.test)
# say(cor(site.test$value, pcr.pred, method="pearson"))
Model.rho[length(Model.rho)+1] <- cor(site.test$value, pcr.pred, method="pearson")
Model.sp[length(Model.sp)+1] <- cor(site.test$value, pcr.pred, method="spearman")
names(Model.rho)[length(Model.rho)] <- "PCA"

say("PLSR")
plsr.fit = plsr(grouped.formula, data = site.train)  # from pls package
plsr.pred = predict(plsr.fit, ncomp = 5, newdata = site.test)
#say(cor(site.test$value, plsr.pred, method="pearson"))
Model.rho[length(Model.rho)+1] <-cor(site.test$value, plsr.pred, method="pearson")
Model.sp[length(Model.sp)+1] <-cor(site.test$value, plsr.pred, method="spearman")
names(Model.rho)[length(Model.rho)] <- "PLSR"






names(Model.sp) = names(Model.rho)


FeatureSelection["fwd-ind",GetFetures(best.formula)] <- 1
FeatureSelection["StepBIC",gsub(" ","",GetFetures(as.character(formula(stepbic.fit)[3])))] <- 1
FeatureSelection["StepAIC",gsub(" ","",GetFetures(as.character(formula(step.fit)[3])))] <- 1

subDir = paste0("./tmp_ModelData_",Version)

if (!file.exists(subDir)){
  dir.create( subDir )
}

write.table(FeatureSelection , file = paste0(subDir,"/FeatureSelection_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
write.table(t(as.data.frame(Model.rho)) , file = paste0(subDir,"/Model.rho_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
write.table(t(as.data.frame(Model.sp)) , file = paste0(subDir,"/Model.spearman_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
write.table(tune , file = paste0(subDir,"/SVMtune_",filenum,".txt"), quote = F, col.names = T, row.names = T, sep = "\t")
