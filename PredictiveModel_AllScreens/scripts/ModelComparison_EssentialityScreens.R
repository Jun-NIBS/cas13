#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))



###### sub-routines #################################

########################## sub-routines end  ########

###### execute ######################################


# load data
HEK = read.delim( '../HEKessentialityScreen/data/HEK_ModelComparison_Results.txt' , sep = ' ' , header = T, stringsAsFactors = F )
A375 = read.delim( '../A375essentialityScreen/data/A375_ModelComparison_Results.txt' , sep = ' ' , header = T, stringsAsFactors = F )
df = rbind.data.frame(HEK,A375)
df = df[grep( 'percent' , df$Measure),]
df$Model = factor(df$Model , levels = c("RFgfp", "RFcombined") )

g = ggplot(df , aes( x = Cells, y = SpearmanCoef, fill = Model))+
  geom_bar(position = 'dodge', stat = 'identity')+
  theme_classic() +
  scale_fill_manual( values = c('#4292c6','#08519c') ) +
  coord_fixed(ratio = 10)

pdf("./figures/Fig3d_ModelComparison_EssentialityScreens.pdf", width = 3, height = 4, useDingbats = F)
grid.arrange( g )
dev.off()