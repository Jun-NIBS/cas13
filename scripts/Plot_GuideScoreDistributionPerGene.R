#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(GenomicRanges, quietly = T))
suppressMessages(library(dplyr, quietly = T))



###### sub-routines #################################

GetCDSregionBounderies = function(x){
  
  out = as.numeric(gsub("CDS:","", strsplit(x , split = "-")[[1]]))
  
  if (length(out) != 2){
    return(NULL)
  }
  else{
    return(out)
  }
}

GetCoord = function(x,y=2){
  as.numeric(gsub( "CDS:","",strsplit(x, split = "-")[[1]][y]))
}


AnnotateExons = function(x=Guide.df, z=info){
  
  coords = GetExonCoords(z)
  chr = rep(1,nrow(coords))
  coords = cbind.data.frame(chr,GetExonCoords(z))
  gr.ex = makeGRangesFromDataFrame( df = coords, ignore.strand = T, keep.extra.columns = F , seqnames.field = "chr", start.field = "Ex.starts", end.field = "Ex.ends")
  
  x$chr = 1
  gr.guides = makeGRangesFromDataFrame( df = x, ignore.strand = T, keep.extra.columns = F , seqnames.field = "chr", start.field = "MatchPos", end.field = "MatchPos")
  
  OL = findOverlaps(gr.guides , gr.ex)
  ex.id = subjectHits(OL)
  return(ex.id)
  
}

Dist2ExonsJunctions = function(x=Guide.df, z=info){
  
  coords = GetExonCoords(z)

  EJs = coords$Ex.ends[ c( 1 : (nrow(coords)-1) )]
  
  D = list()
  for ( i in 1:length(EJs)){
    D[[i]] = x$MatchPos - EJs[i]
  }
  ma = do.call(cbind, D)
  
  colnames(ma) = paste0("EJ",seq(1,length(D),1))
  rownames(ma) = x$GuideName
  
  return(ma)
  
}

GetExonCoords = function(info){
  EX = as.numeric(strsplit(info$exons, split = ";")[[1]])
  if (length(EX) == 1){
    Ex.starts = 1
    Ex.ends = EX[1]
    Ex.mid = round(Ex.ends/2)
  }else{
    Ex.starts = c(1)
    Ex.ends = c(EX[1])
    Ex.mid = round(Ex.ends/2)
    for (i in 2:length(EX)){
      Ex.starts[i] = Ex.starts[i-1] + EX[i-1]
      Ex.ends[i] = Ex.ends[i-1] + EX[i]
      Ex.mid[i] = Ex.ends[i]  - round(EX[i]/2)
    }
  }
  Ex.ID = seq(1,length(Ex.starts),1)
  coords = cbind.data.frame( Ex.ID , Ex.starts , Ex.ends, Ex.mid)
  return(coords)
}

Annotate = function(x=Guide.df$MatchPos, z=info){
  
  s = GetCoord(z$cds_coord, y = 1)
  e = GetCoord(z$cds_coord, y = 2)
  
  u5 = which(as.numeric(x) < s)
  cds = which(as.numeric(x) >= s & as.numeric(x) <= e)
  u3 = which(as.numeric(x) > e)
  
  out = rep(NA, length(x))
  
  if(length(u5) > 0){
    out[u5] = as.character("5`UTR")
  }
  if(length(cds) > 0){
    out[cds] = as.character("CDS")
  }
  if(length(u3) > 0){
    out[u3] = as.character("3`UTR")
  }
  
  return(out)
  
}

AssignQuartiles = function(x,q = quantile(x)){
  quartiles = vector()
  for ( i in 1:length(x)){
    if (is.na(x[i]) == T){
      quartiles[i] = NA
    }
    else if (x[i] <= q["25%"]){
      quartiles[i] = 1
    }
    else if (x[i] > q["25%"] & x[i] <= q["50%"]){
      quartiles[i] = 2
    }
    else if (x[i] > q["50%"] & x[i] <= q["75%"]){
      quartiles[i] = 3
    }
    else if (x[i] > q["75%"]){
      quartiles[i] = 4
    }
    else{
      stop("Exiting. Value outside quartiles")
    }
  }
  return(quartiles) 
}


PlotGuideDist <- function( df = CD46 , info = NULL , f=0.2 , VALUE = "meanCS" , plotCDS=FALSE ){
  
  if ( is.null(info)){
    LEN = max(df$MatchPos, na.rm = T)
    if(LEN <= 500){b=25}else if(LEN > 500 & LEN <= 1000 ){b=50}else if(LEN > 1000 & LEN <= 2000 ){b=100}else if(LEN > 2000 & LEN <= 5000 ){b=250}else if(LEN > 5000 & LEN <= 10000 ){b=500}else{b=1000}
    name = "eGFP"
    colnames(df)[grep(VALUE, colnames(df))] = "value"
    # add quartile info
    df$quartiles = AssignQuartiles(df$value, q = quantile(df$value, na.rm=T))
    df = filter(df, !is.na(quartiles))
    df$quartiles = factor(df$quartiles , levels = rev(sort(as.numeric(unique(df$quartiles)))))
    
    Y = sum(abs(range(df$value , na.rm = T)))
    Ymax = max(df$value , na.rm = T)
    Ymin = min(df$value , na.rm = T)
    
    ymins = c(quantile(df$value , probs = c(0, 0.25, 0.5 , 0.75) , na.rm=T) )
    ymaxs = c(quantile(df$value , probs = c(0.25, 0.5 , 0.75, 1) , na.rm=T) )
    s = c( ((ymins[1] - ymaxs[1])/2) + ymaxs[1]  , (ymaxs[2] + ymins[2])/2 , ((ymaxs[3] - ymins[3])/2) + ymins[3] , ((ymaxs[4] - ymins[4])/2) + ymins[4])
    names(s) = NULL
    
    coords = cbind.data.frame(1,1,LEN,LEN/2)
    colnames(coords) = c("Ex.ID", "Ex.starts", "Ex.ends", "Ex.mid")
    
    g = ggplot(df, aes(x=MatchPos, y=value)) +
      geom_point(shape=20,  aes(color = quartiles))  +
      geom_smooth(span = 0.05 , method = "loess", formula = y~x, colour = "#525252" ) +
      theme_classic() +
      scale_color_manual(values = rev(c('#ca0020','#f4a582','#92c5de','#0571b0'))) +
      ggtitle(name) +
      ylab(VALUE) + xlab("guide match position [nt]") +
      coord_fixed(ratio=(LEN/Y)*f) +
      scale_x_continuous(breaks = seq(0,LEN,b)) +
      theme(text = element_text(size=12)) +
      geom_rect(data=coords, inherit.aes=FALSE, aes(xmin=Ex.starts, xmax=Ex.ends, ymin=Ymin, ymax=Ymin + (Y*0.05)), color="#525252", fill="#045a8d", alpha=0.1, size = 0.1) + 
      annotate( geom = "text" , x = coords$Ex.mid , y = Ymin + (Y*0.025) , label = coords$Ex.ID, hjust = 0.5, vjust = 0.5, size = 2.5) +
      annotate( geom = "text" , x = -25 , y = Ymin + (Y*0.025) , label = "exons", hjust = 1, vjust = 0.5, size = 3) +
      annotate( "rect", xmin= -(LEN*0.05), xmax=-25, ymin=ymins, ymax=ymaxs , color="transparent", fill=c('#ca0020','#f4a582','#92c5de','#0571b0'), alpha=0.1) +
      annotate( geom = "text" , x = rep( (-(LEN*0.05) + 25)/2-25 ,4)  , y = s , label = c("Q1","Q2","Q3","Q4"), hjust = 0.5, vjust = 0.5, size = 3 ) 
    
    return(g)
  }
  else if (plotCDS){
    
    #annotate
    df$Annotation = NA
    df$Annotation = Annotate( df$MatchPos , z=info)
    df$Exon = NA
    df$Exon = AnnotateExons( df , z=info)
    #other info
    CDS = info$cds_coord
    cds=data.frame(matrix(GetCDSregionBounderies(CDS), ncol = 2))
    colnames(cds) = c("start","end")
    LEN = (cds[1,2]+50) - (cds[1,1]-50)
    df = df[which(df$MatchPos >= (cds[1,1]-50) & df$MatchPos <= (cds[1,2]+50)  ),]
    name = paste0(info$gene_name, " : " , info$transcript_id)
    coords = GetExonCoords(info)
    del = which( coords$Ex.ends < cds[1,1] | coords$Ex.starts > cds[1,2] )
    if (length(del) > 0){
      coords = coords[-del,]
    }    
    coords$Ex.ID[which( coords$Ex.starts < (cds[1,1]-50) )] = ""
    coords$Ex.mid[which( coords$Ex.starts < (cds[1,1]-50) )] = coords$Ex.ends[which( coords$Ex.starts < (cds[1,1]-50) )]
    coords$Ex.starts[which( coords$Ex.starts < (cds[1,1]-50) )] = (cds[1,1]-50)
    if( length(which( coords$Ex.ends > (cds[1,2]+50) )) > 0){
      coords$Ex.ID[which( coords$Ex.ends > (cds[1,2]+50) )] = ""
      coords$Ex.mid[which( coords$Ex.ends > (cds[1,2]+50) )] =  coords$Ex.starts[which( coords$Ex.ends > (cds[1,2]+50) )]
      coords$Ex.ends[which( coords$Ex.ends > (cds[1,2]+50) )] = (cds[1,2]+50)
    }
    
    
    
    if(LEN <= 500){b=25}else if(LEN > 500 & LEN <= 1000 ){b=50}else if(LEN > 1000 & LEN <= 2000 ){b=100}else if(LEN > 2000 & LEN <= 5000 ){b=250}else if(LEN > 5000 & LEN <= 10000 ){b=500}else{b=1000}
    
    colnames(df)[grep(VALUE, colnames(df))] = "value"
    
    # add quartile info
    df$quartiles=NA
    df$quartiles[which( df$Annotation == "CDS")] = AssignQuartiles(df$value[which( df$Annotation == "CDS")], q = quantile(df$value[which( df$Annotation == "CDS")], na.rm=T))
    df$quartiles[which( is.na(df$quartiles) == TRUE)] = "n.d."
    df$quartiles = factor(df$quartiles , levels = c( "4", "3", "2", "1", "n.d.") )
    
    Y = sum(abs(range(df$value , na.rm = T)))
    Ymax = max(df$value , na.rm = T)
    Ymin = min(df$value , na.rm = T)
    
    ymins = c(quantile(df$value , probs = c(0, 0.25, 0.5 , 0.75) , na.rm=T) )
    ymaxs = c(quantile(df$value , probs = c(0.25, 0.5 , 0.75, 1) , na.rm=T) )
    xmin = min(df$MatchPos, na.rm=TRUE)
    xmax = max(df$MatchPos, na.rm=TRUE)
    s = c( ((ymins[1] - ymaxs[1])/2) + ymaxs[1]  , (ymaxs[2] + ymins[2])/2 , ((ymaxs[3] - ymins[3])/2) + ymins[3] , ((ymaxs[4] - ymins[4])/2) + ymins[4])
    names(s) = NULL
    
    g = ggplot(df, aes(x=MatchPos, y=value)) +
      geom_point(shape=20,  aes(color = quartiles))  +
      geom_smooth(span = 0.05 , method = "loess", formula = y~x, colour = "#525252" ) +
      theme_classic() +
      scale_color_manual(values = rev(c('#969696','#ca0020','#f4a582','#92c5de','#0571b0'))) +
      ggtitle(name) +
      ylab(VALUE) + xlab("guide match position [nt]") +
      coord_fixed(ratio=(LEN/Y)*f) +
      scale_x_continuous(breaks = seq(as.integer(xmin/100)*100,round(xmax/100)*100,b)) +
      theme(text = element_text(size=12)) +
      geom_rect(data=cds, inherit.aes=FALSE, aes(xmin=start, xmax=end, ymin=Ymin, ymax=Ymax), color="transparent", fill="#045a8d", alpha=0.1) +
      annotate(geom = "text" , x = cds$start+25 , y = Ymin + (Y * 0.1) , label = "CDS", hjust = 0, vjust = 0) +
      geom_rect(data=coords, inherit.aes=FALSE, aes(xmin=Ex.starts, xmax=Ex.ends, ymin=Ymin, ymax=Ymin + (Y*0.05)), color="#525252", fill="#045a8d", alpha=0.1, size = 0.1) + 
      annotate( geom = "text" , x = coords$Ex.mid , y = Ymin + (Y*0.025) , label = coords$Ex.ID, hjust = 0.5, vjust = 0.5, size = 2.5) +
      annotate( geom = "text" , x = xmin-25 , y = Ymin + (Y*0.025) , label = "exons", hjust = 1, vjust = 0.5, size = 3) +
      annotate( "rect", xmin= (xmin-25)-(LEN*0.05), xmax=xmin-25, ymin=ymins, ymax=ymaxs , color="transparent", fill=c('#ca0020','#f4a582','#92c5de','#0571b0'), alpha=0.1) +
      annotate( geom = "text" , x = rep( (xmin-25)-(LEN*0.05)/2 + 25 ,4)  , y = s , label = c("Q1","Q2","Q3","Q4"), hjust = 1, vjust = 0.5, size = 3 ) 
  
    
    return(g)
    
  }
  else{
    
    #annotate
    df$Annotation = NA
    df$Annotation = Annotate( df$MatchPos , z=info)
    df$Exon = NA
    df$Exon = AnnotateExons( df , z=info)
    #other info
    LEN = unique(info$length)
    CDS = info$cds_coord
    cds=data.frame(matrix(GetCDSregionBounderies(CDS), ncol = 2))
    colnames(cds) = c("start","end")
    name = paste0(info$gene_name, " : " , info$transcript_id)
    coords = GetExonCoords(info)
    if(LEN <= 500){b=25}else if(LEN > 500 & LEN <= 1000 ){b=50}else if(LEN > 1000 & LEN <= 2000 ){b=100}else if(LEN > 2000 & LEN <= 5000 ){b=250}else if(LEN > 5000 & LEN <= 10000 ){b=500}else{b=1000}
    
    colnames(df)[grep(VALUE, colnames(df))] = "value"
    
    # add quartile info
    df$quartiles = AssignQuartiles(df$value, q = quantile(df$value, na.rm=T))
    df = filter(df, !is.na(quartiles))
    df$quartiles = factor(df$quartiles , levels = rev(sort(as.numeric(unique(df$quartiles)))))
    
    Y = sum(abs(range(df$value , na.rm = T)))
    Ymax = max(df$value , na.rm = T)
    Ymin = min(df$value , na.rm = T)
    
    ymins = c(quantile(df$value , probs = c(0, 0.25, 0.5 , 0.75) , na.rm=T) )
    ymaxs = c(quantile(df$value , probs = c(0.25, 0.5 , 0.75, 1) , na.rm=T) )
    s = c( ((ymins[1] - ymaxs[1])/2) + ymaxs[1]  , (ymaxs[2] + ymins[2])/2 , ((ymaxs[3] - ymins[3])/2) + ymins[3] , ((ymaxs[4] - ymins[4])/2) + ymins[4])
    names(s) = NULL
    
    g = ggplot(df, aes(x=MatchPos, y=value)) +
      geom_point(shape=20,  aes(color = quartiles))  +
      geom_smooth(span = 0.025 , method = "loess", formula = y~x, colour = "#525252" ) +
      theme_classic() +
      scale_color_manual(values = rev(c('#ca0020','#f4a582','#92c5de','#0571b0'))) +
      ggtitle(name) +
      ylab(VALUE) + xlab("guide match position [nt]") +
      coord_fixed(ratio=(LEN/Y)*f) +
      scale_x_continuous(breaks = seq(0,LEN,b)) +
      theme(text = element_text(size=12)) +
      geom_rect(data=cds, inherit.aes=FALSE, aes(xmin=start, xmax=end, ymin=Ymin, ymax=Ymax), color="transparent", fill="#045a8d", alpha=0.1) +
      annotate(geom = "text" , x = cds$start+25 , y = Ymin + (Y * 0.1) , label = "CDS", hjust = 0, vjust = 0) +
      geom_rect(data=coords, inherit.aes=FALSE, aes(xmin=Ex.starts, xmax=Ex.ends, ymin=Ymin, ymax=Ymin + (Y*0.05)), color="#525252", fill="#045a8d", alpha=0.1, size = 0.1) + 
      annotate( geom = "text" , x = coords$Ex.mid , y = Ymin + (Y*0.025) , label = coords$Ex.ID, hjust = 0.5, vjust = 0.5, size = 2.5) +
      annotate( geom = "text" , x = -25 , y = Ymin + (Y*0.025) , label = "exons", hjust = 1, vjust = 0.5, size = 3) +
      annotate( "rect", xmin= -(LEN*0.05), xmax=-25, ymin=ymins, ymax=ymaxs , color="transparent", fill=c('#ca0020','#f4a582','#92c5de','#0571b0'), alpha=0.1) +
      annotate( geom = "text" , x = rep( (-(LEN*0.05) + 25)/2-25 ,4)  , y = s , label = c("Q1","Q2","Q3","Q4"), hjust = 0.5, vjust = 0.5, size = 3 ) 
    
    # geom_label_repel(aes(label=ifelse(Rank <= topN ,Rank,'')),
    #                  box.padding   = 0.25, 
    #                  point.padding = 0.25,
    #                  label.size = 0.1,
    #                  nudge_y      = 0.15,
    #                  direction = "both",
    #                  segment.color = 'grey50')
    
    return(g)
    
  }
  
  
  
}


PlotGuideAtExonBounderies <- function( df = CD46 , info = info.cd46 , VALUE = "meanCS" , LIMIT = 50 , TITLE = "" , DATA = FALSE , SHIFT = TRUE , GuideLen=23 ){
  
  name = paste0(info$gene_name, " : " , info$transcript_id)
  
  #annotate
  df$Annotation = NA
  df$Annotation = Annotate( df$MatchPos , z=info)
  df$Exon = NA
  df$Exon = AnnotateExons( df , z=info)
  
  Dist = as.data.frame(Dist2ExonsJunctions(df , z=info))
  Dist$score = df[,grep(VALUE, colnames(df))]
  DF = melt(Dist, id.vars = "score")
  DF$variable = gsub("EJ", "", DF$variable)
  colnames(DF) = c("value" , "ExonJunction", "Distance")
  
  if(SHIFT){
    DF$Distance = DF$Distance - round(GuideLen/2)
  }
  
  DF = DF[which( abs(DF$Distance) < LIMIT ),]
  
  if(DATA){
    DF$Gene = TITLE
    return(DF)
    
  }
  
  X1 = 2*LIMIT
  Ymax1 = max( DF$value , na.rm = T)
  Ymin1 = min( DF$value , na.rm = T)   
  Y1 = sum(abs(range( DF$value , na.rm = T)))
  R1 = X1/Y1/1.5
  
  Breaks = seq(-100,100,10)
  Breaks = Breaks[which(abs(Breaks) <= LIMIT)]
  
  # g = ggplot( DF , aes(x=Distance, y = value)) + 
  #   geom_boxplot(aes(group = Distance), fill = "#f0f0f0",outlier.shape=20, outlier.size=-1) + 
  #   geom_smooth(method = 'loess' , se = TRUE, span = 0.125 , fill = "#d9d9d9") +
  #   # geom_jitter( aes(fill = ExonJunction , color = ExonJunction) , shape = 21, size = 1, width = 0.2 )
  #   theme_classic() + ylab(VALUE) + xlab("distance to Exon Junction [nt]") +  ggtitle(TITLE) +
  #   coord_fixed(ratio = R1) +
  #   geom_hline(yintercept=0, linetype="dashed", color = "#252525") +
  #   annotate("rect", xmin = -LIMIT, xmax = 0, ymin = Ymin1, ymax = Ymin1-(Ymax1-Ymin1)*0.05, alpha = 0.5 , color = '#252525', size = 0.35)+
  #   annotate("rect", xmin = 0, xmax = LIMIT, ymin = Ymin1, ymax = Ymin1-(Ymax1-Ymin1)*0.05, alpha = 0.5 , color = '#252525', size = 0.35)+
  #   annotate("text" , x = -LIMIT/2 , y = Ymin1-(Ymax1-Ymin1)*0.025 , label = "5' exon", color = "#ffffff", size = 2)+
  #   annotate("text" , x = LIMIT/2 , y = Ymin1-(Ymax1-Ymin1)*0.025 , label = "3' exon", color = "#ffffff", size = 2) +
  #   scale_x_continuous(breaks = Breaks)
  
  DF$Q = AssignQuartiles( x = DF$value , q = quantile(DF$value))
  DF$Q = factor(DF$Q , levels = c(4:1))
  
  g = ggplot( DF , aes(x=Distance, y = value)) + 
    geom_point(color = "#252525",shape=20) + 
    geom_smooth( aes( group = Q , color = Q) , method = 'loess' , se = TRUE, span = 0.125 , fill = "#d9d9d9") +
    theme_classic() + ylab(VALUE) + xlab("distance to Exon Junction [nt]") +  ggtitle(TITLE) +
    coord_fixed(ratio = R1) +
    geom_hline(yintercept=0, linetype="dashed", color = "#252525") +
    scale_color_manual( values= c( '#67000d','#cb181d','#fb6a4a','#fcbba1') ) +
    annotate("rect", xmin = -limit, xmax = 0, ymin = Ymin1, ymax = Ymin1-(Ymax1-Ymin1)*0.05, alpha = 0.5 , color = '#252525', size = 0.35)+
    annotate("rect", xmin = 0, xmax = limit, ymin = Ymin1, ymax = Ymin1-(Ymax1-Ymin1)*0.05, alpha = 0.5 , color = '#252525', size = 0.35)+
    annotate("text" , x = -limit/2 , y = Ymin1-(Ymax1-Ymin1)*0.025 , label = "5' exon", color = "#ffffff", size = 2)+
    annotate("text" , x = limit/2 , y = Ymin1-(Ymax1-Ymin1)*0.025 , label = "3' exon", color = "#ffffff", size = 2)+
    scale_x_continuous(breaks = Breaks)
  return(g)
  
}

PlotOverview = function(df = All , value = "ScaledCS" , TITLE = ""){
  df$Annotation = factor( df$Annotation , levels = c("5`UTR","CDS","3`UTR","Intron"))
  df$Screen = factor( df$Screen , levels = c("GFP","CD46","CD55","CD71"))
  colnames(df)[which(colnames(df) == value)] = "value"
  
  g1 = ggplot(df , aes( x = Annotation , y = value, fill = Annotation)) + 
    geom_violin() +
    geom_boxplot(outlier.size = -1, width = 0.33, fill = "white") +
    theme_classic() + 
    ylab(value) + 
    ggtitle("Combined") +
    scale_fill_manual(values=c( "#fec44f", "#3182bd", "#de2d26", "#41ab5d"))+
    theme(plot.title = element_text(size = 10)) +
    geom_hline( yintercept = 0 , linetype = "dashed" , color = "#525252")
  
  g2 = ggplot(df , aes( x = Annotation , y = value, fill = Annotation)) + 
    facet_wrap( Screen ~ . , nrow = 1) +
    geom_violin() +
    geom_boxplot(outlier.size = -1, width = 0.33, fill = "white") +
    theme_classic() + 
    ylab(value) + 
    ggtitle(TITLE) +
    scale_fill_manual(values=c( "#fec44f", "#3182bd", "#de2d26", "#41ab5d"))+
    theme(plot.title = element_text(size = 10)) +
    geom_hline( yintercept = 0 , linetype = "dashed" , color = "#525252")
  
  df2 = df
  df2$Screen = "Combined"
  DF = rbind.data.frame(df,df2)
  DF$Screen = factor( DF$Screen , levels = c("Combined","GFP","CD46","CD55","CD71"))
  
  g3 = ggplot(DF , aes( x = Annotation , y = value, fill = Annotation)) + 
    facet_wrap( Screen ~ . , nrow = 1) +
    geom_violin() +
    geom_boxplot(outlier.size = -1, width = 0.33, fill = "white") +
    theme_classic() + 
    ylab(value) + 
    ggtitle(TITLE) +
    scale_fill_manual(values=c( "#fec44f", "#3182bd", "#de2d26", "#41ab5d"))+
    theme(plot.title = element_text(size = 10)) +
    geom_hline( yintercept = 0 , linetype = "dashed" , color = "#525252")
  
  return(list(g1,g2,g3))
}


AssignQuartiles = function(x,q = quantile(x)){
  quartiles = vector()
  for ( i in 1:length(x)){
    if (is.na(x[i]) == T){
      quartiles[i] = NA
    }
    else if (x[i] <= q["25%"]){
      quartiles[i] = 1
    }
    else if (x[i] > q["25%"] & x[i] <= q["50%"]){
      quartiles[i] = 2
    }
    else if (x[i] > q["50%"] & x[i] <= q["75%"]){
      quartiles[i] = 3
    }
    else if (x[i] > q["75%"]){
      quartiles[i] = 4
    }
    else{
      stop("Exiting. Value outside quartiles")
    }
  }
  return(quartiles) 
}

########################## sub-routines end  ########

###### execute ######################################

# Get Info data
INFO = read.delim('./data/GeneInfo.tsv' , sep = '\t', header = T, stringsAsFactors = F)

# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)


# Make GuideNames unique
# Results$Guide = paste0(Results$Screen,":",gsub("crRNA","",gsub("crRNA0","",gsub("crRNA00","",Results$Guide))))

# Restrict to single Screens
Screens = split(Results, f = Results$Screen)
GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]

# Scale Data individually
GFP$ScaledCS = scale(GFP$meanCS , center = T)[,1]
CD46$ScaledCS = scale(CD46$meanCS , center = T)[,1]
CD55$ScaledCS = scale(CD55$meanCS , center = T)[,1]
CD71$ScaledCS = scale(CD71$meanCS , center = T)[,1]
All = rbind.data.frame(GFP,CD46,CD55,CD71)

# Plot Guide efficacy in sub mRNA annotation categories
GFP = GFP[ which( GFP$Annotation %in% c("3`UTR", "5`UTR","CDS", "Intron" ) & GFP$MatchType %in% c("Perfect Match" , "Intronic")),]
CD46 = CD46[which( CD46$Annotation %in% c("3`UTR", "5`UTR","CDS", "Intron" ) & CD46$MatchType %in% c("Perfect Match" , "Intronic")),]
CD55 = CD55[which( CD55$Annotation %in% c("3`UTR", "5`UTR","CDS", "Intron" ) & CD55$MatchType %in% c("Perfect Match" , "Intronic")),]
CD71 = CD71[which( CD71$Annotation %in% c("3`UTR", "5`UTR","CDS", "Intron" ) & CD71$MatchType %in% c("Perfect Match" , "Intronic")),]
All = All[which( All$Annotation %in% c("3`UTR", "5`UTR","CDS", "Intron" ) & All$MatchType %in% c("Perfect Match" , "Intronic")),]

p = PlotOverview(df = All , value = "ScaledCS" , TITLE = "")

lay = rbind(c(1,1,2,2,2,2,2),
            c(3,3,3,3,3,3,3))
pdf("./figures/FigS9f_GuidesmRNAannotation.pdf", width = 7, height = 5, useDingbats = F)
grid.arrange(grobs = p , layout_matrix = lay)
dev.off()








# Select Perfect matching
GFP = GFP[grep("Perfect Match", GFP$MatchType),]
CD46 = CD46[grep("Perfect Match", CD46$MatchType),]
CD55 = CD55[grep("Perfect Match", CD55$MatchType),]
CD71 = CD71[grep("Perfect Match", CD71$MatchType),]
All = All[grep("Perfect Match", All$MatchType),]


# Get Target Sequence
gfp.fa = "./PreProcessingMetaData/data/GFP.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_ENST00000367042.1.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_ENST00000367064.3.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_ENST00000360110.4.fa"
FA.GFP <- Biostrings::readDNAStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
FA.CD46 <- Biostrings::readDNAStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
FA.CD55 <- Biostrings::readDNAStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
FA.CD71 <- Biostrings::readDNAStringSet(filepath = cd71.fa, format = "fasta", use.names = T)
info.cd46 = INFO[ which(INFO$transcript_id == names(FA.CD46)) , ]
info.cd55 = INFO[ which(INFO$transcript_id == names(FA.CD55)) , ]
info.cd71 = INFO[ which(INFO$transcript_id == names(FA.CD71)) , ]


g1 = PlotGuideDist(df = GFP , info = NULL , f=0.2 , VALUE = "meanCS")
g2 = PlotGuideDist(df = CD46 , info = info.cd46 , f=0.2 , VALUE = "meanCS")
warning("removes 1 point in CD55 3'utr for plotting reasons")
del=which(CD55$meanCS == max(CD55$meanCS))
g3 = PlotGuideDist(df = CD55[-del,] , info = info.cd55 , f=0.2 , VALUE = "meanCS")
g4 = PlotGuideDist(df = CD71 , info = info.cd71 , f=0.2 , VALUE = "meanCS")

# pdf("./figures/FigZZ_GuidesDistribution.pdf", width = 12, height = 12, useDingbats = F)
# grid.arrange(grobs = list( g1,g2,g3,g4 ) , ncol = 1 )
# dev.off()



g2 = PlotGuideDist(df = CD46 , info = info.cd46 , f=0.2 , VALUE = "meanCS" , plotCDS = TRUE)
del=which(CD55$meanCS == max(CD55$meanCS))
g3 = PlotGuideDist(df = CD55[-del,] , info = info.cd55 , f=0.2 , VALUE = "meanCS" , plotCDS = TRUE) 
warning("removes 1 point in CD55 3'utr for plotting reasons")
g4 = PlotGuideDist(df = CD71 , info = info.cd71 , f=0.2 , VALUE = "meanCS" , plotCDS = TRUE)

pdf("./figures/Fig1d_Fig3abc_GuidesDistribution_CDSonly.pdf", width = 12, height = 12, useDingbats = F)
grid.arrange(grobs = list( g1,g2,g3,g4 ) , ncol = 1 )
dev.off()

limit = 60
VALUE = "ScaledCS"
g1 = PlotGuideAtExonBounderies( df = CD46 , info = info.cd46 , VALUE = VALUE , LIMIT = limit , TITLE = "CD46" , DATA = F , SHIFT = TRUE)
g2 = PlotGuideAtExonBounderies( df = CD55 , info = info.cd55 , VALUE = VALUE , LIMIT = limit , TITLE = "CD55"  , DATA = F , SHIFT = TRUE)
g3 = PlotGuideAtExonBounderies( df = CD71 , info = info.cd71 , VALUE = VALUE , LIMIT = limit , TITLE = "CD71"  , DATA = F , SHIFT = TRUE)
data.cd46 = PlotGuideAtExonBounderies( df = CD46 , info = info.cd46 , VALUE = VALUE , LIMIT = limit , TITLE = "CD46" , DATA = TRUE, SHIFT = TRUE)
data.cd55 = PlotGuideAtExonBounderies( df = CD55 , info = info.cd55 , VALUE = VALUE , LIMIT = limit , TITLE = "CD55"  , DATA = TRUE, SHIFT = TRUE)
data.cd71 = PlotGuideAtExonBounderies( df = CD71 , info = info.cd71 , VALUE = VALUE , LIMIT = limit , TITLE = "CD71"  , DATA = TRUE, SHIFT = TRUE)
data.all = rbind(data.cd46 , data.cd55, data.cd71)

X1 = 2*limit
Ymax1 = max( data.all$value , na.rm = T)
Ymin1 = min( data.all$value , na.rm = T)   
Y1 = sum(abs(range( data.all$value , na.rm = T)))
R1 = X1/Y1/1.5

data.all$Q = AssignQuartiles( x = data.all$value , q = quantile(data.all$value))
data.all$Q = factor(data.all$Q , levels = c(4:1))
data.all$Color = NA
data.all$Color = ifelse( data.all$Q == 4 , "Q4" , "other")

Breaks = seq(-100,100,10)
Breaks = Breaks[which(abs(Breaks) <= limit)]

g4 = ggplot( data.all , aes(x=Distance, y = value)) + 
  geom_point(aes(fill = Color) ,shape=21) + 
  geom_smooth( aes( fill = NA, group = Q , color = Q) , method = 'loess' , se = TRUE, span = 0.125 ) +
  theme_classic() + ylab(VALUE) + xlab("distance to Exon Junction [nt]") +  ggtitle("Combined") +
  coord_fixed(ratio = R1) +
  geom_hline(yintercept=0, linetype="dashed", color = "#252525") +
  scale_fill_manual( values = c('#969696', '#252525') ) +
  scale_color_manual( values= c( '#05539c','#3081bd','#6aacd5','#bdd7e7') )+
  annotate("rect", xmin = -limit, xmax = 0, ymin = Ymin1, ymax = Ymin1-(Ymax1-Ymin1)*0.05, alpha = 0.5 , color = '#252525', size = 0.35)+
  annotate("rect", xmin = 0, xmax = limit, ymin = Ymin1, ymax = Ymin1-(Ymax1-Ymin1)*0.05, alpha = 0.5 , color = '#252525', size = 0.35)+
  annotate("text" , x = -limit/2 , y = Ymin1-(Ymax1-Ymin1)*0.025 , label = "5' exon", color = "#ffffff", size = 2)+
  annotate("text" , x = limit/2 , y = Ymin1-(Ymax1-Ymin1)*0.025 , label = "3' exon", color = "#ffffff", size = 2)+
  scale_x_continuous(breaks = Breaks)









pdf("./figures/FigS9h_ExonJunction.pdf", width = 4.5, height = 14, useDingbats = F)
grid.arrange(grobs = list( g4,g2,g3,g1 ) , ncol = 1 )
dev.off()


