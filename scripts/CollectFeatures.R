#!/usr/bin/env Rscript


###### set variables; load libs #####################


.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.4")
suppressMessages(library(Biostrings, quietly = T))
suppressMessages(library(ppcor, quietly = T))
suppressMessages(library(qgraph, quietly = T))




###### subroutines ##################################


ExtractMFEInfo = function(x){
  Name <- as.character(sapply( x , extractName))
  MFE <- as.numeric(sapply( x , extractMFE))
  Fold <- as.character(sapply( x , extractFOLD))
  Gquad <- grepl("\\+",Fold) 
  Gquad = ifelse( Gquad == TRUE , 1, 0)
  eval <-  sapply( Fold  , evalFOLD)  
  MFE.df =  cbind.data.frame(Name,MFE,  Fold, eval , Gquad)
  colnames(MFE.df)  = c("Name","MFE","Fold","DR","Gquad")
  return(MFE.df)
}
MatchDFs = function(  x=GFP,  MFE = MFE.df.gfp){
  idx <- match(  as.character(x$GuideName) , as.character(MFE$Name)) #match by name
  if (all(as.character(x$GuideName) ==  as.character(MFE$Name)[idx])){
    x$MFE <- MFE$MFE[idx] # add MFE
    x$DR <- MFE$DR[idx] # add Fold evaluation
    x$Gquad <- MFE$Gquad[idx] # add Fold evaluation
    return(x)
  }
  else{
    stop("exiting. Input files cannot be combined like this.")
  }
}


extractName <- function(x){
  if(length(strsplit(x, split = "-")[[1]]) == 2){
    tmp=strsplit(x, split = "-")[[1]][1]
    out=paste0(strsplit(tmp, split = "_")[[1]], collapse = "_")
    return(out)
  }
  else{
    tmp=paste0(strsplit(x, split = "-")[[1]][1:2], collapse = "-")
    out=paste0(strsplit(tmp, split = "_")[[1]], collapse = "_")
    return(out)    
  }
}


extractMFE <- function(x){
  if(length(strsplit(x, split = "-")[[1]]) == 2){
    tmp=strsplit(x, split = "-")[[1]][2]
    out=as.numeric(strsplit(tmp, split = "_")[[1]][1])
    return(-out)
  }
  else{
    tmp=strsplit(x, split = "-")[[1]][3]
    out=as.numeric(strsplit(tmp, split = "_")[[1]][1])
    return(-out)    
  }
}

extractFOLD = function(x){
  l = length(strsplit(x, split = "-")[[1]])
  
  tmp=strsplit(x, split = "-")[[1]][l]
  out=strsplit(tmp, split = "_")[[1]][2]
  
  return(out)
  
}

evalFOLD <- function(x){
  if(substr(x, 1, 24) == "((((((.(((....))).))))))"){
    return(1)
  }
  else{
    return(0)
  }
}
extractREADID <- function(x){
  strsplit(x, split = "_")[[1]][1]
}






Get_crRNAfold = function(x, MFE = MFE.df){
  idx <- match(  rownames(x) , MFE$Name) #match by name
  if (all(rownames(x) == MFE$Name[idx])){
    x$MFE.crRNA <- MFE$MFE.crRNA[idx] # add MFE
    x$Fold.crRNA <- MFE$Fold.crRNA[idx] # add Fold
    x$DirectRepeat <- MFE$DirectRepeat[idx] # add Fold evaluation
    x$Gquadruplex <- MFE$Gquad[idx] # add Fold evaluation
  }
  else{
    stop("Exiting! Guides are not in the correct order")
  }
  return(x)
}



GetMatch = function(j,y = FASTA, VAL = "pos", MM=1 , TARGET = "GFP"){
  if (TARGET == "GFP"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$pHKO32_GFP
  }
  else if(TARGET == "CD46"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$ENST00000367042.1
  }
  else if(TARGET == "CD55"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$ENST00000367064.3
  }
  else if(TARGET == "CD71"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$ENST00000360110.4
  }else{
    stop("exiting. please refine the target gene. Use \"GFP\",\"CD46\",\"CD55\" or \"CD71\".")
  }
  
  if (length(res) == 0){
    return("Error. No match found")
  }
  else if (length(res) == 1){
    if (VAL == "pos"){
      return(end(res))
    }
    else if (VAL == "TargetSeq"){
      str=substring(y,start(res)-4,end(res)+4)
      
      if (nchar(str) < 4){
        return(NA)
      }
      else{
        return(str)
      }
    }
    else if (VAL == "upstream"){
      str=substring(y,end(res)+1,end(res)+4)
      
      if (nchar(str) < 4){
        return(NA)
      }
      else{
        return(str)
      }
      
    }
    
    else if (VAL == "downstream"){
      str=substring(y,start(res)-4,start(res)-1)
      
      if (nchar(str) < 4){
        return(NA)
      }
      else{
        return(str)
      }
    }
  }
  else{
    return(print(c("Error. More than 1 match found for ", j)))
  }
}



GetTargetNucleotides <- function(x = GFP, G = GUIDES.GFP, FASTA=FA.GFP, PERSPECTIVE = "target", guideLen = 23){
  

  if( grepl( "GFP" , names(FASTA)) ){
    target="GFP"
  }else if( grepl( "ENST00000367042" , names(FASTA)) ){
    target="CD46"
  }else if( grepl( "ENST00000367064" , names(FASTA)) ){
    target="CD55"
  }else if( grepl( "ENST00000360110" , names(FASTA)) ){
    target="CD71"
  }
  else{
    target="GFP"
  }
  

  
  
  
  idx = match( x$GuideName , names(G))
  
  if (all( x$GuideName == names(G)[idx])){
    
    # trim GuideSeq for GFP screen from 27 to 23
    if(width(G[idx[1]]) > 23){
      x$GuideSeq <- as.character(subseq( x = G[idx] , start = 1, end = guideLen))
    }else{
      x$GuideSeq <- as.character(G[idx])
    }
    
    
    x$RetrievedPos <- sapply(x$GuideSeq ,GetMatch, y = FASTA, VAL = "pos" , MM=0, TARGET = target)
    
    
    
    if (all(x$MatchPos == x$RetrievedPos)){
      
      dat = x

      dat$TargetSeq <- sapply(dat$GuideSeq ,GetMatch, y = FASTA, VAL = "TargetSeq" , MM=0 , TARGET = target )
      
      if (PERSPECTIVE == "target"){
        
        # split the retrieved sequence by nucleotide. The sequence has -4 and +4 nucleotides surrounding the Cas13d guide match. The sequence represents the target sequence, that is the reverse complement of the guide sequence 
        ma <- as.data.frame(do.call(rbind,sapply(dat$TargetSeq, FUN = function(y){strsplit(y,split="")}))) # produces a warning, that can be ignored, and will be corrected downstream
        cat("\nproduces a warning, that can be ignored, and will be corrected downstream\n\n")
        colnames(ma) <- c("-4","-3","-2","-1",seq(guideLen,1,-1),"+1","+2","+3","+4")
        rownames(ma) <- rownames(dat) # add rownames
        ma$matchPos  <- dat$MatchPos # add match positions 


        offset = 4 # set length of offset surrounding the guide
        WINDOW = (guideLen+(2*offset))
        SeqLen = width(FASTA)
        
        # As some guide may fall at the target edges, we need to correct the that by assigning NAs to upstream and downstream regions
        # initialize output matrix
        out = as.data.frame(matrix(NA, ncol = WINDOW, nrow = nrow(dat) ))
        colnames(out) =  colnames(ma)[1:WINDOW]
        rownames(out) = rownames(ma)
        out$matchPos  <- ma$matchPos
        ma <- as.matrix(ma)
        out <- as.matrix(out)
        
        # correct nucleotide positions of target sites for target site falling close to target edges
        for ( i in 1:nrow(ma)){
          
          # if close to target start, move the sequence 'to the right', and add NAs to the first positions, if non-matching
          if (as.numeric(ma[i,"matchPos"]) < (guideLen + offset)){
            
            o = ( guideLen + offset) - as.numeric(ma[i,"matchPos"])
            
            out[i,((o+1):(offset + guideLen + offset) )] <- ma[i,(1:(as.numeric(ma[i,"matchPos"])+offset))]
            
            ma[i, ] <- out[i,]
            
          }
          # if close to target end, no move of the sequence required, but add NAs to the last positions, if non-matching
          else if (as.numeric(ma[i,"matchPos"]) > (SeqLen-offset)){
            
            o = (as.numeric(ma[i,"matchPos"])+offset) -  SeqLen
            
            ma[i,(offset + guideLen + offset - (o-1)):(offset + guideLen + offset) ] <- NA
            
            
          }
          else{
            next
          }
        }
        
        remove(out) # remove intermediate file
        ma <- as.data.frame(ma) # transform to data frame
        del = grep("matchPos",colnames(ma))
        out = cbind.data.frame(dat,ma[,-del])

        return(out)
  
      }
      
      else{
        stop("the corresponding guide perspective hasn't been implemented yet")
      }
      
      return(g)
      
    }
    else{
      stop("not all guides match positions are identical")
    }
    
  }
  else{
    stop("not all guides match")
  }
}


GetType<-function(x){
  if(grepl("FirstOrder",x)){
    return("First Order")
  }
  else if (grepl("consecTriple",x)){
    return("Consecutive Triple")
  }
  else if (grepl("randomDouble",x)){
    return("Random Double")
  }
  else if (grepl("rc_",x)){
    return("Non-Targeting")
  }
  else if (grepl("consecDouble",x)){
    return("Consecutive Double")
  }
  else if (grepl("LengthVariant",x)){
    return("Length Variant")
  }
  else if (grepl("intron",x)){
    return("Intronic")
  }
  else if (grepl("Rev",x)){
    return("Reverse Complement")
  }
  else{
    return("Perfect Match")
  }
}



GetLetterFreq = function(x = GFP, G=GUIDES.GFP, S=1, E=23){
  
  types = sapply(names(G) , GetType)
  m = which(types == "Perfect Match")
  G = G[m]
  
  G.sub = subseq( G,start = S,end = E)
  G = G.sub
  
  AU.freq <- letterFrequency( G , letters = c("AT") ,as.prob = F)
  GC.freq <- letterFrequency( G , letters = c("GC") ,as.prob = F)
  AU.prob <- letterFrequency( G , letters = c("AT") ,as.prob = T)
  GC.prob <- letterFrequency( G , letters = c("GC") ,as.prob = T)
  A.prob <- letterFrequency( G , letters = c("A") ,as.prob = T)
  C.prob <- letterFrequency( G , letters = c("C") ,as.prob = T)
  G.prob <- letterFrequency( G , letters = c("G") ,as.prob = T)
  U.prob <- letterFrequency( G , letters = c("T") ,as.prob = T)
  A.freq <- letterFrequency( G , letters = c("A") ,as.prob = F)
  C.freq <- letterFrequency( G , letters = c("C") ,as.prob = F)
  G.freq <- letterFrequency( G , letters = c("G") ,as.prob = F)
  U.freq <- letterFrequency( G , letters = c("T") ,as.prob = F)
  diNucleotide.prob <- dinucleotideFrequency(G, as.prob = T)
  diNucleotide.freq <- dinucleotideFrequency(G, as.prob = F)
  
  
  
  out <- cbind.data.frame(A.freq,C.freq,G.freq,U.freq,A.prob,C.prob,G.prob,U.prob,GC.freq,AU.freq,GC.prob,AU.prob,diNucleotide.freq,diNucleotide.prob)
  rownames(out) <- names(G)
  colnames(out) <- c("A","C","G","T","pA","pC","pG","pT","G|C", "A|T", "pG|pC", "pA|pT", "AA","AC","AG","AT","CA","CC","CG","CT","GA","GC",
                     "GG","GT","TA","TC","TG","TT","pAA","pAC","pAG","pAT","pCA","pCC","pCG","pCT","pGA","pGC","pGG","pGT","pTA","pTC","pTG","pTT")
  
  idx = match( x$GuideName , rownames(out) )
  
  if (all(x$GuideName == rownames(out)[idx])) {
    
    return(cbind.data.frame(x,out[idx,]))

  }else{
    stop('exiting. guide names do not match!')
  }
  
  
}


GetNTdensitities = function(fa = FA.target.GFP){
  NTs = c("A","C","G","T","AT","GC")
  ma.A = Get_NT_density_Vector(fa = fa, NT = NTs[1] , WINDOW = max[which(max$NT  == NTs[1]),"W"] )
  ma.C = Get_NT_density_Vector(fa = fa, NT = NTs[2] , WINDOW = max[which(max$NT  == NTs[2]),"W"] )
  ma.G = Get_NT_density_Vector(fa = fa, NT = NTs[3] , WINDOW = max[which(max$NT  == NTs[3]),"W"] )
  ma.T = Get_NT_density_Vector(fa = fa, NT = NTs[4] , WINDOW = max[which(max$NT  == NTs[4]),"W"] )
  ma.AT = Get_NT_density_Vector(fa = fa, NT = NTs[5] , WINDOW = max[which(max$NT == NTs[5]),"W"] )
  ma.GC = Get_NT_density_Vector(fa = fa, NT = NTs[6] , WINDOW = max[which(max$NT == NTs[6]),"W"] )
  
  mi.A = Get_NT_density_Vector(fa = fa, NT = NTs[1] , WINDOW = min[which(min$NT  == NTs[1]),"W"] )
  mi.C = Get_NT_density_Vector(fa = fa, NT = NTs[2] , WINDOW = min[which(min$NT  == NTs[2]),"W"] )
  mi.G = Get_NT_density_Vector(fa = fa, NT = NTs[3] , WINDOW = min[which(min$NT  == NTs[3]),"W"] )
  mi.T = Get_NT_density_Vector(fa = fa, NT = NTs[4] , WINDOW = min[which(min$NT  == NTs[4]),"W"] )
  mi.AT = Get_NT_density_Vector(fa = fa, NT = NTs[5] , WINDOW = min[which(min$NT == NTs[5]),"W"] )
  mi.GC = Get_NT_density_Vector(fa = fa, NT = NTs[6] , WINDOW = min[which(min$NT == NTs[6]),"W"] )
  
  L = list(ma.A,ma.C,ma.G,ma.T,ma.AT,ma.GC  ,  mi.A,mi.C,mi.G,mi.T,mi.AT,mi.GC)
  names(L) = c(paste0('max_',NTs),paste0('min_',NTs)) 
  return(L)
}

Get_NT_density_Vector = function(fa = FA,NT = "G", WINDOW = 30){
  
  D = WINDOW
  
  ma <- rep(NA,ncol = width(fa))  
  
  
  if((D %% 2) == 0) {
    
    d = D/2
    
    for (p in 1:width(fa)){
      
      if ( (p-(d-1)) < 1 | (p+d) > width(fa) ){
        ma[p] <- NA
      }
      else{
        ma[p] <-  letterFrequency( subseq(fa, start=p-(d-1), end=p+d)  , letters = NT ,as.prob = T) 
      }
    }
  } 
  
  else {
    
    d = (D-1)/2
    for (p in 1:width(fa)){
      if ( (p-d) < 1 | (p+d) > width(fa) ){
        ma[p] <- NA
      }
      else{
        ma[p] <-  letterFrequency( subseq(fa, start=p-d, end=p+d)  , letters = NT ,as.prob = T) 
      }
    }
  }
  
  return(ma)
}

Get_NT_density_Matrix <- function( fa = FA , NT = "G"){
  
  
  ma <- matrix(NA,ncol = width(fa), nrow = 50 )  
  colnames(ma) <- seq(1,width(fa),1)
  rownames(ma) <- paste0("d",seq(1,50,1))
  for (D in 1:50){
    
    if((D %% 2) == 0) {
      
      d = D/2
      
      for (p in 1:width(fa)){
        
        if ( (p-(d-1)) < 1 | (p+d) > width(fa) ){
          ma[D,p] <- NA
        }
        else{
          ma[D,p] <-  letterFrequency( subseq(fa, start=p-(d-1), end=p+d)  , letters = NT ,as.prob = T) 
        }
      }
    } 
    
    
    
    else {
      
      d = (D-1)/2
      for (p in 1:width(fa)){
        if ( (p-d) < 1 | (p+d) > width(fa) ){
          ma[D,p] <- NA
        }
        else{
          ma[D,p] <-  letterFrequency( subseq(fa, start=p-d, end=p+d)  , letters = NT ,as.prob = T) 
        }
      }
    }
  }
  
  return(ma)
  
}


GetValue <- function(x,d,w){ 
  if (is.null(nrow(x)) == T){
    return(NA)
  }
  else{
  return(x[d,w])
  }
}


GetVal = function(j,vec=VEC, POINT = -11){
  
  # if j is NA, return vector of NAs
  if (is.na(j) == T){
    return(NA)
  }
  
  else{
    if ((j + POINT) < 1){
      return(NA)
    }
    else{
      return(vec[j + POINT])
    }
  }
}

SliceMatrix <- function(j,mat=MA, w=50){
  
  if (is.na(j) == T){
    tmp = matrix(NA, ncol = 2*w+1, nrow = nrow(mat))
    colnames(tmp) <- seq(-w,w,1)
    rownames(tmp) <- rownames(mat)
  }
  else{
    tmp = matrix(NA, ncol = length((j-w):(j+w)), nrow = nrow(mat))
    rownames(tmp) <- rownames(mat)
    
    if ((j-w) < 1){
      
      tmp[,( ((w+1)-j+1):ncol(tmp))] <-  mat[,1:(j+w)]
      
    }
    else if ((j+w) > ncol(mat)){
      
      tmp[,( 1 : ( (w+1) + (ncol(mat) - j)) )] <-  mat[,(j-w):(ncol(mat))]
      
    }
    else{
      
      tmp <- mat[,(j-w):(j+w)]
      
    }
    
    colnames(tmp) <- seq(-w,w,1)
    return(tmp)
  }
}   

GetLocalNTcontent <- function( x=Results , MA = ma.C, W=100, X=90 , Y=22){
  
  # slice the nucleotide density matrix for each guide to obtain a window of +/- the window size (default 50nt) centered on guide match position 1
  Slices = lapply( as.list(x$MatchPos) , FUN = SliceMatrix ,mat=MA, w=W)
  names(Slices) <- rownames(x)
  
  dens <- sapply(Slices , FUN = GetValue, d = Y, w = X)
  
  return(dens)
  
}  

Transform_RNAplfold_predictions <- function(x){
  
  
  ma <- matrix(NA,ncol = ncol(x), nrow = nrow(x) )  
  colnames(ma) <- colnames(x)
  rownames(ma) <- rownames(x)
  for (i in 1:nrow(x)){
    
    if((i %% 2) == 0) {
      
      d = (i/2)-1
      
      ma[i, 1:(ncol(x)-d) ]  <- x[i, (1+d):ncol(x) ]
      
    } 
    else {
      
      d = (i-1)/2
      
      ma[i, 1:(ncol(x)-d) ]  <- x[i, (1+d):ncol(x) ]
      
    }
  }
  return(ma)
}

GetNTfeatureTable <- function(dat = TargetNucleotides.gfp){

  from = grep("-4", colnames(dat))
  ma = dat[,from:ncol(dat)]
  pos = colnames(ma)
  
  lbls = paste0(c("A","C","G","T"),"_",  rep(pos,each=4))
  out = matrix(0, ncol = length(lbls), nrow = nrow(ma))
  colnames(out) = lbls
  rownames(out) = rownames(ma)
  
  for ( i in 1:nrow(ma)){
    for (k in 1:length(pos)){
      if (is.na(ma[i,k]) == T & colnames(ma)[k] %in% c("-4","-3","-2","-1","+1","+2","+3","+4" )){
        out[i,grep(colnames(ma)[k],colnames(out))]
      }
      else if (is.na(ma[i,k]) == T & !(colnames(ma)[k] %in% c("-4","-3","-2","-1","+1","+2","+3","+4" ))){
        stop("Exiting! Unexpected NA value")
      }
      else{
        if (length(which(colnames(out) == paste0( ma[i,k] ,"_", colnames(ma)[k]))) == 1){
          out[i,which(colnames(out) == paste0( ma[i,k] ,"_", colnames(ma)[k]))] <- 1
        }
        else{
          stop("Exiting! Refine matching")
        }
      }
    }
  }
  
  return(cbind.data.frame(dat,out))
}



Get_diNT_featureTable <- function(dat = TargetNucleotides.onehot){
  
  from = grep("TargetSeq", colnames(dat))+1
  tmp = dat[,from:(from+30)]
  
  diNT = matrix(NA, ncol = ncol(tmp)-1, nrow(tmp))
  colnames(diNT) = colnames(tmp)[-1]
  rownames(diNT) = rownames(tmp)
  for (i in 2:ncol(tmp)){
    diNT[,i-1] <- paste0(tmp[,(i-1)],tmp[,i])
  }
  
  
  pos = colnames(diNT)
  lbls = paste0(c("AA","AC","AG","AT","CA","CC","CG","CT","GA","GC","GG","GT","TA","TC","TG","TT"),"_",rep(pos,each=16))
  out = matrix(0, ncol = length(lbls), nrow = nrow(diNT))
  colnames(out) = lbls
  rownames(out) = rownames(diNT)
  
  for ( i in 1:nrow(diNT[1:10,])){
    for (k in 1:length(pos)){
      if (grepl("NA",diNT[i,k]) == T & colnames(diNT)[k] %in% c("-3","-2","-1","27","+1","+2","+3","+4" )){
        out[i,grep(colnames(diNT)[k],colnames(out))] <- NA
      }
      else if (grepl("NA",diNT[i,k]) == T & !(colnames(diNT)[k] %in% c("-3","-2","-1","27","+1","+2","+3","+4" ))){
        stop("Exiting! Unexpected NA value")
      }
      else{
        if (length(which(colnames(out) == paste0( diNT[i,k] ,"_", colnames(diNT)[k]))) == 1){
          out[i,which(colnames(out) == paste0(diNT[i,k] ,"_", colnames(diNT)[k]))] <- 1
        }
        else{
          stop("Exiting! Refine matching")
        }
      }
    }
  }
  
  return(cbind.data.frame( dat,out))
}

ReadUnpairedPorbabilities = function(x){
  UnpairedProbabilities <- read.delim(x, sep="\t", skip = 1, row.names = "X.i.")[,1:50]
  colnames(UnpairedProbabilities) <- seq(1,50,1)
  UnpairedProbabilities=t(UnpairedProbabilities)
  return(UnpairedProbabilities)
}

GetNTpointdensities = function(DAT = CD71 , Vec.list = NTdensitities.cd71){
  
  ma.A = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_A"]] , POINT = max[which(max$NT  == "A"),"P"])
  ma.C = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_C"]] , POINT = max[which(max$NT  == "C"),"P"])
  ma.G = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_G"]] , POINT = max[which(max$NT  == "G"),"P"])
  ma.T = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_T"]] , POINT = max[which(max$NT  == "T"),"P"])
  ma.AT = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_AT"]] , POINT = max[which(max$NT  == "AT"),"P"])
  ma.GC = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_GC"]] , POINT = max[which(max$NT  == "GC"),"P"])
  
  mi.A = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["min_A"]] , POINT = min[which(min$NT  == "A"),"P"])
  mi.C = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["min_C"]] , POINT = min[which(min$NT  == "C"),"P"])
  mi.G = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["min_G"]] , POINT = min[which(min$NT  == "G"),"P"])
  mi.T = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["min_T"]] , POINT = min[which(min$NT  == "T"),"P"])
  mi.AT = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["min_AT"]] , POINT = min[which(min$NT  == "AT"),"P"])
  mi.GC = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["min_GC"]] , POINT = min[which(min$NT  == "GC"),"P"])
  L = list(ma.A,ma.C,ma.G,ma.T,ma.AT,ma.GC    ,   mi.A,mi.C,mi.G,mi.T,mi.AT,mi.GC)
  names(L) = paste0( "NTdens_" , names(Vec.list))
  dens = do.call( cbind , L)
  out = cbind.data.frame( DAT , dens)
  return(out)
}


GetNTdensitities_GFP = function(fa = FA.target.GFP){
  NTs = c("A","C","G","T","uU")
  ma.A = Get_NT_density_Vector(fa = fa, NT = NTs[1] , WINDOW = gfp[which(gfp$NT  == NTs[1]),"W"] )
  ma.C = Get_NT_density_Vector(fa = fa, NT = NTs[2] , WINDOW = gfp[which(gfp$NT  == NTs[2]),"W"] )
  ma.G = Get_NT_density_Vector(fa = fa, NT = NTs[3] , WINDOW = gfp[which(gfp$NT  == NTs[3]),"W"] )
  ma.T = Get_NT_density_Vector(fa = fa, NT = NTs[4] , WINDOW = gfp[which(gfp$NT  == NTs[4]),"W"] )
  ma.uU = Get_NT_density_Vector(fa = fa, NT = NTs[4] , WINDOW = gfp[which(gfp$NT == NTs[5]),"W"] )
  
  L = list(ma.A,ma.C,ma.G,ma.T,ma.uU)
  names(L) = c(paste0('max_',NTs)) 
  return(L)
}
GetNTpointdensities_GFP = function( DAT = GFP , Vec.list = NTdensitities.gfp ){
  
  ma.A = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_A"]] , POINT = gfp[which(gfp$NT  == "A"),"P"])
  ma.C = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_C"]] , POINT = gfp[which(gfp$NT  == "C"),"P"])
  ma.G = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_G"]] , POINT = gfp[which(gfp$NT  == "G"),"P"])
  ma.T = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_T"]] , POINT = gfp[which(gfp$NT  == "T"),"P"])
  ma.uU = sapply( as.list(DAT$MatchPos) , FUN = GetVal ,vec=Vec.list[["max_uU"]] , POINT = gfp[which(gfp$NT  == "uU"),"P"])
  
  L = list(ma.A,ma.C,ma.G,ma.T,ma.uU)
  names(L) = paste0( "GFP_local_" , names(Vec.list))
  dens = do.call( cbind , L)
  out = cbind.data.frame( DAT , dens)
  return(out)
}
###### execute ######################################




##################  Load ModelInput
Results = read.delim('./data/NormalizedCombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)


# Restrict to single Screens
Screens = split(Results, f = Results$Screen)
GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]





##################  Get crRNA fold MFE

# Get crRNA MFE
#load file # takes only 1 min for 5mio reads
# gfp.fa = "./PreProcessingMetaData/data/Cas13d_GFP_library.final.crRNAplusDR.foldedTransformed.fa"
gfp.fa = "./PreProcessingMetaData/data/Cas13d_GFP_library_23mer.final.crRNAplusDR.foldedTransformed.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_library.final.crRNAplusDR.foldedTransformed.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_library.final.crRNAplusDR.foldedTransformed.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_library.final.crRNAplusDR.foldedTransformed.fa"
mfe.GFP  <- Biostrings::readBStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
mfe.CD46  <- Biostrings::readBStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
mfe.CD55  <- Biostrings::readBStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
mfe.CD71  <- Biostrings::readBStringSet(filepath = cd71.fa, format = "fasta", use.names = T)


# read out RNAfold output
MFE.df.gfp = ExtractMFEInfo(names(mfe.GFP))
MFE.df.cd46 = ExtractMFEInfo(names(mfe.CD46))
MFE.df.cd55 = ExtractMFEInfo(names(mfe.CD55))
MFE.df.cd71 = ExtractMFEInfo(names(mfe.CD71))

# match 
GFP = MatchDFs(x=GFP,  MFE = MFE.df.gfp)
CD46 = MatchDFs(x=CD46,  MFE = MFE.df.cd46)
CD55 = MatchDFs(x=CD55,  MFE = MFE.df.cd55)
CD71 = MatchDFs(x=CD71,  MFE = MFE.df.cd71)



   
##################  Get Target folding

UnpairedProbabilities.gfp = ReadUnpairedPorbabilities(x = "./PreProcessingMetaData/data/pHKO32_GFP_lunp")
UnpairedProbabilities.cd46 = ReadUnpairedPorbabilities(x = "./PreProcessingMetaData/data/ENST00000367042.1_lunp")
UnpairedProbabilities.cd55 = ReadUnpairedPorbabilities(x = "./PreProcessingMetaData/data/ENST00000367064.3_lunp")
UnpairedProbabilities.cd71 = ReadUnpairedPorbabilities(x = "./PreProcessingMetaData/data/ENST00000360110.4_lunp")

UnpairedProbabilities.gfp.tranformed <- Transform_RNAplfold_predictions(UnpairedProbabilities.gfp)
UnpairedProbabilities.cd46.tranformed <- Transform_RNAplfold_predictions(UnpairedProbabilities.cd46)
UnpairedProbabilities.cd55.tranformed <- Transform_RNAplfold_predictions(UnpairedProbabilities.cd55)
UnpairedProbabilities.cd71.tranformed <- Transform_RNAplfold_predictions(UnpairedProbabilities.cd71)

# As there was no clear pattern, this will only record the unpaired probability covering the entire guide match
GFP$Log10_Unpaired <- GetLocalNTcontent(x=GFP , MA = log10(UnpairedProbabilities.gfp.tranformed), W=50, X=40 , Y=23)
CD46$Log10_Unpaired <- GetLocalNTcontent(x=CD46 , MA = log10(UnpairedProbabilities.cd46.tranformed), W=50, X=40 , Y=23)  
CD55$Log10_Unpaired <- GetLocalNTcontent(x=CD55 , MA = log10(UnpairedProbabilities.cd55.tranformed), W=50, X=40 , Y=23)  
CD71$Log10_Unpaired <- GetLocalNTcontent(x=CD71 , MA = log10(UnpairedProbabilities.cd71.tranformed), W=50, X=40 , Y=23)  





##################  Get guide:target hybridization MFE

cors = read.delim('./data/RNAhybCorrelations.txt', sep = '\t', header = T, stringsAsFactors = F)
comb = cors[grep("Combined",cors$Screen),]
max = comb[grep("max",comb$COR),]
min = comb[grep("min",comb$COR),]

# load pre-computed RNAhybridization energies between crRNA and Target site
RNAhyb.gfp = read.table('./PreProcessingMetaData/data/GFP_HybridizationMatrix_merged_All.csv', sep=",")
RNAhyb.cd46 = read.table('./PreProcessingMetaData/data/CD46_HybridizationMatrix_merged_All.csv', sep=",")
RNAhyb.cd55 = read.table('./PreProcessingMetaData/data/CD55_HybridizationMatrix_merged_All.csv', sep=",")
RNAhyb.cd71 = read.table('./PreProcessingMetaData/data/CD71_HybridizationMatrix_merged_All.csv', sep=",")

# select comlumn
col.max.gfp = which(colnames(RNAhyb.gfp) == paste0( "X" , max$Position, ".", max$Width  ))
col.min.gfp = which(colnames(RNAhyb.gfp) == paste0( "X" , min$Position, ".", min$Width  ))
col.max.oth = which(colnames(RNAhyb.cd71) == paste0( "X" , max$Position, ".", max$Width  ))
col.min.oth = which(colnames(RNAhyb.cd71) == paste0( "X" , min$Position, ".", min$Width  ))

# extract
hyb.gfp = RNAhyb.gfp[,c(col.max.gfp,col.min.gfp)]
hyb.cd46 = RNAhyb.cd46[,c(col.max.oth,col.min.oth)]
hyb.cd55 = RNAhyb.cd55[,c(col.max.oth,col.min.oth)]
hyb.cd71 = RNAhyb.cd71[,c(col.max.oth,col.min.oth)]

colnames(hyb.gfp) = gsub("X","_",paste0("hybMFE",colnames(hyb.gfp)))
colnames(hyb.cd46) = gsub("X","_",paste0("hybMFE",colnames(hyb.cd46)))
colnames(hyb.cd55) = gsub("X","_",paste0("hybMFE",colnames(hyb.cd55)))
colnames(hyb.cd71) = gsub("X","_",paste0("hybMFE",colnames(hyb.cd71)))

# match by guide name
GFP = cbind.data.frame( GFP , hyb.gfp[ match(GFP$GuideName , rownames(hyb.gfp)) , ])
CD46 = cbind.data.frame( CD46 , hyb.cd46[ match(CD46$GuideName , rownames(hyb.cd46)) , ])
CD55 = cbind.data.frame( CD55 , hyb.cd55[ match(CD55$GuideName , rownames(hyb.cd55)) , ])
CD71 = cbind.data.frame( CD71 , hyb.cd71[ match(CD71$GuideName , rownames(hyb.cd71)) , ])




##################  Get Target Nucleotide densities

cors = read.delim('./data/LocalNTdensityCorrelations.txt', sep = '\t', header = T, stringsAsFactors = F)
comb = cors[grep("combined",cors$Screen),]
max = comb[grep("max",comb$COR),]
min = comb[grep("min",comb$COR),]

# Read Target fasta sequence
FA.target.GFP <- Biostrings::readDNAStringSet(filepath = "./data/GFP_ext.fa", format = "fasta", use.names = T)
FA.target.CD46 <- Biostrings::readDNAStringSet(filepath = "./PreProcessingMetaData/data/CD46_ENST00000367042.1.fa", format = "fasta", use.names = T)
FA.target.CD55 <- Biostrings::readDNAStringSet(filepath = "./PreProcessingMetaData/data/CD55_ENST00000367064.3.fa", format = "fasta", use.names = T)
FA.target.CD71 <- Biostrings::readDNAStringSet(filepath = "./PreProcessingMetaData/data/CD71_ENST00000360110.4.fa", format = "fasta", use.names = T)


# Get Target nucleotide densities
# This caluculates only the specific vector of the nt-density matrix that I am interested in
NTdensitities.gfp = GetNTdensitities(FA.target.GFP)
NTdensitities.cd46 = GetNTdensitities(FA.target.CD46)
NTdensitities.cd55 = GetNTdensitities(FA.target.CD55)
NTdensitities.cd71 = GetNTdensitities(FA.target.CD71)


GFP = GetNTpointdensities( DAT = GFP , Vec.list = NTdensitities.gfp )
CD46 = GetNTpointdensities( DAT = CD46 , Vec.list = NTdensitities.cd46  )
CD55 = GetNTpointdensities( DAT = CD55 , Vec.list = NTdensitities.cd55  )
CD71 = GetNTpointdensities( DAT = CD71 , Vec.list = NTdensitities.cd71  )



##################  For Comparison to initial RFgfp model, get Target Nucleotide densities according to that

gfp = max[1:5,1:4]
gfp$Screen = "GFP"
gfp$NT[5] = "uU"
gfp$P = c(-22,-11,-10,-3,-78)
gfp$W = c(7,22,21,18,30)



# Get Target nucleotide densities
# This caluculates only the specific vector of the nt-density matrix that I am interested in
NTdensitities.gfp = GetNTdensitities_GFP(FA.target.GFP)
NTdensitities.cd46 = GetNTdensitities_GFP(FA.target.CD46)
NTdensitities.cd55 = GetNTdensitities_GFP(FA.target.CD55)
NTdensitities.cd71 = GetNTdensitities_GFP(FA.target.CD71)


GFP = GetNTpointdensities_GFP( DAT = GFP , Vec.list = NTdensitities.gfp )
CD46 = GetNTpointdensities_GFP( DAT = CD46 , Vec.list = NTdensitities.cd46  )
CD55 = GetNTpointdensities_GFP( DAT = CD55 , Vec.list = NTdensitities.cd55  )
CD71 = GetNTpointdensities_GFP( DAT = CD71 , Vec.list = NTdensitities.cd71  )





##################  Write Output

Results_all_Features = rbind.data.frame( GFP , CD46 , CD55 , CD71)

write.table(Results_all_Features , file = "./data/ModelInputFeatures_AllScreens_PerfectMatch.csv", col.names = T, row.names = T, quote = F, sep = ",")




##################  Get Nucleotide Freuqencies


# Get Target Sequence
gfp.fa = "./PreProcessingMetaData/data/GFP.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_ENST00000367042.1.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_ENST00000367064.3.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_ENST00000360110.4.fa"
FA.GFP <- Biostrings::readDNAStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
FA.CD46 <- Biostrings::readDNAStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
FA.CD55 <- Biostrings::readDNAStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
FA.CD71 <- Biostrings::readDNAStringSet(filepath = cd71.fa, format = "fasta", use.names = T)

# Get Guide Sequences
gfp.fa = "./PreProcessingMetaData/data/Cas13d_GFP_library.final.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_library.final.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_library.final.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_library.final.fa"
GUIDES.GFP  <- Biostrings::readDNAStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
GUIDES.CD46  <- Biostrings::readDNAStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
GUIDES.CD55  <- Biostrings::readDNAStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
GUIDES.CD71  <- Biostrings::readDNAStringSet(filepath = cd71.fa, format = "fasta", use.names = T)





TargetNucleotides.gfp <- GetTargetNucleotides(x = GFP, G = GUIDES.GFP, FASTA=FA.GFP , guideLen = 23) # limited to a guide length of 23
TargetNucleotides.cd46 <- GetTargetNucleotides(x = CD46, G = GUIDES.CD46, FASTA=FA.CD46 , guideLen = 23)
TargetNucleotides.cd55 <- GetTargetNucleotides(x = CD55, G = GUIDES.CD55, FASTA=FA.CD55 , guideLen = 23)
TargetNucleotides.cd71 <- GetTargetNucleotides(x = CD71, G = GUIDES.CD71, FASTA=FA.CD71 , guideLen = 23)
TargetNucleotides.all <- rbind.data.frame(TargetNucleotides.gfp,
                                          TargetNucleotides.cd46,
                                          TargetNucleotides.cd55,
                                          TargetNucleotides.cd71)

TargetNucleotides.onehot = GetNTfeatureTable(TargetNucleotides.all)

TargetDiNucleotides.onehot = Get_diNT_featureTable(TargetNucleotides.onehot)


write.table(TargetNucleotides.all,      file = paste0("./data/TargetNucleotides.csv"),          sep = ",", quote = F, row.names = T, col.names = T)
write.table(TargetNucleotides.onehot,   file = paste0("./data/TargetNucleotides_onehot.csv"),   sep = ",", quote = F, row.names = T, col.names = T)
write.table(TargetDiNucleotides.onehot, file = paste0("./data/TargetDiNucleotides_onehot.csv"), sep = ",", quote = F, row.names = T, col.names = T)




##################  Get Nucleotide content/frequency

LetterFreq.gfp = GetLetterFreq(x = GFP, G = GUIDES.GFP, S=1, E=23)
LetterFreq.cd46 = GetLetterFreq(x = CD46, G = GUIDES.CD46, S=1, E=23)
LetterFreq.cd55 = GetLetterFreq(x = CD55, G = GUIDES.CD55, S=1, E=23)
LetterFreq.cd71 = GetLetterFreq(x = CD71, G = GUIDES.CD71, S=1, E=23)
LetterFreq.all <- rbind.data.frame(LetterFreq.gfp,
                                   LetterFreq.cd46,
                                   LetterFreq.cd55,
                                   LetterFreq.cd71)


LetterFreqSeed.gfp  = GetLetterFreq(x = GFP,  G = GUIDES.GFP,  S=15, E=22)
LetterFreqSeed.cd46 = GetLetterFreq(x = CD46, G = GUIDES.CD46, S=15, E=22)
LetterFreqSeed.cd55 = GetLetterFreq(x = CD55, G = GUIDES.CD55, S=15, E=22)
LetterFreqSeed.cd71 = GetLetterFreq(x = CD71, G = GUIDES.CD71, S=15, E=22)
LetterFreqSeed.all <- rbind.data.frame(LetterFreqSeed.gfp,
                                       LetterFreqSeed.cd46,
                                       LetterFreqSeed.cd55,
                                       LetterFreqSeed.cd71)

write.table( LetterFreq.all, file = paste0("./data/LetterFreq.csv"), sep = ",", quote = F, row.names = T, col.names = T)
write.table( LetterFreqSeed.all, file = paste0("./data/LetterFreqSeed.csv"), sep = ",", quote = F, row.names = T, col.names = T)




if ( all(LetterFreq.all[,1:12] == TargetDiNucleotides.onehot[1:12])){
  del = which( colnames(TargetDiNucleotides.onehot) %in% colnames(LetterFreq.all))
  CompleteFeatureSet = cbind.data.frame(LetterFreq.all , TargetDiNucleotides.onehot[,-del])
  
  write.table( CompleteFeatureSet, file = paste0("./data/CompleteFeatureSet.csv"), sep = ",", quote = F, row.names = T, col.names = T)
}



##################  Plot 'on-target' feature output

# PM = split(Results,Results$type)[[1]]
# 
# p1 = ggplot(PM, aes(x=MatchPos, y=meanCS.BIN1)) +
#   geom_point(shape=20) + geom_smooth(span = 0.05 , method = "loess", formula = y~x) +
#   theme_classic() +
#   ggtitle("Perfect Match") + 
#   ylim(c(-1,1.5)) + ylab("CRISPR score") + xlab("guide match position [nt]") +
#   coord_fixed(ratio=(714/2.5)/10) + 
#   scale_x_continuous(breaks = seq(25,700,25))
# 
# p2 = ggplot(PM, aes(x=MatchPos, y=MFE.crRNA)) +
#   geom_point(shape=20) + geom_smooth(span = 0.05 , method = "loess", formula = y~x) +
#   theme_classic() +
#   ggtitle("") + 
#   ylim(c(-30,-15)) + ylab("crRNA MFE") + xlab("guide match position [nt]") +
#   coord_fixed(ratio=(714/15)/10) + 
#   scale_x_continuous(breaks = seq(25,700,25))
# 
# p3 = ggplot(PM, aes(x=MatchPos, y=hybMFE_1.26)) +
#   geom_point(shape=20) + geom_smooth(span = 0.05 , method = "loess", formula = y~x) +
#   theme_classic() + ylim(c(-75,-50)) +
#   ggtitle("") + 
#   ylab("hybrid MFE") + xlab("guide match position [nt]") +
#   coord_fixed(ratio=(714/25)/10) + 
#   scale_x_continuous(breaks = seq(25,700,25))
# 
# p4 = ggplot(PM, aes(x=MatchPos, y=Local.C)) +
#   geom_point(shape=20) + geom_smooth(span = 0.05 , method = "loess", formula = y~x) +
#   theme_classic() + ylim(c(0.1,0.6)) +
#   ggtitle("") + 
#   ylab("local C context") + xlab("guide match position [nt]") +
#   coord_fixed(ratio=(714/0.5)/10) + 
#   scale_x_continuous(breaks = seq(25,700,25))
# 
# p5 = ggplot(PM, aes(x=MatchPos, y=Local.upstreamU)) +
#   geom_point(shape=20) + geom_smooth(span = 0.05 , method = "loess", formula = y~x) +
#   theme_classic() + ylim(c(0.0,0.3)) +
#   ggtitle("") + 
#   ylab("upstream U context") + xlab("guide match position [nt]") +
#   coord_fixed(ratio=(714/0.3)/10) + 
#   scale_x_continuous(breaks = seq(25,700,25))
# 
# p6 = ggplot(PM, aes(x=MatchPos, y=Log10_Unpaired.prob_1)) +
#   geom_point(shape=20) + geom_smooth(span = 0.05 , method = "loess", formula = y~x) +
#   theme_classic() + ylim(c(-13,0)) +
#   ggtitle("") + 
#   ylab("accessibility") + xlab("guide match position [nt]") +
#   coord_fixed(ratio=(714/13)/10) + 
#   scale_x_continuous(breaks = seq(25,700,25))
# 
# pdf("Fig2c.pdf", width = 10, height = 2, useDingbats = F)
# grid.arrange(p1, ncol = 1)
# dev.off()
# 
# pdf("FigS10.pdf", width = 11, height = 10, useDingbats = F)
# grid.arrange(p1,p2,p3,p4,p5,p6, ncol = 1)
# dev.off()


##################  Plot correlation and partial correlations for 'on-target' features


# FIELDS = c(colnames)
# 
# # remove incomplete entries
# PM = Results[Results$type == "Perfect Match",]
# PM = PM[complete.cases(PM[,FIELDS]),FIELDS]
# 
# PM = apply(PM, MARGIN = 2, FUN = function(x){as.numeric(as.character(x))})
# 
# PCC = cor(PM)
# pPCC = pcor(PM)$estimate
# 
# d <- 0.1
# cutG <- .25
# maxG <- .8
# Lbls = c(names)
# pdf("FigS12f.pdf",height = 4,width=8,useDingbats = F)
# par(mfrow=c(1,2), pty="s")
# qgraph(PCC, minimum=d, cut = cutG, rescale=F,labels=Lbls,  maximum=maxG, edge.label.cex=2, legend = F, borders = T,  posCol = "darkblue", 
#        negCol = "red",vsize=c(15,rep(10,length(Lbls)-1)), layout = "circular", edge.labels = TRUE,title="correlations")
# 
# 
# qgraph(pPCC, minimum=d, cut = cutG, rescale=F,labels=Lbls,  maximum=maxG, edge.label.cex=2, legend = F, borders = T, posCol = "darkblue", 
#        negCol = "red",vsize=c(15,rep(10,length(Lbls)-1)), layout = "circular", edge.labels = TRUE,title="partial correlations")
# dev.off()