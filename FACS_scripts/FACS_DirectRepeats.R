#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
# install.packages(c("EnvStats","ggplot2","reshape2","ggridges","gridExtra","grid") , lib = "~/R/x86_64-redhat-linux-gnu-library/3.5", dependencies = T)
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggridges, quietly = T))
suppressMessages(library(EnvStats, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))

Calculate_gMFI=function(a){
  m = abs(min(a))+1
  return(geoMean(a+m))}

Assign_Guide_Mod <- function(x, ID = ID.mat, col = 2){
  row = which( ID[,1] == x)
  return(ID[row,col])
}

GetTreatment = function(x){tmp = strsplit(x , split = "_")[[1]]
paste0(tmp[1:(length(tmp)-1)], collapse = "_")}

ExtractElement = function(x,y=1){
  strsplit( x , split = "_")[[1]][y]
}

SelectTopCells = function(x,p = P, side = "top"){
  if (side == "top"){
    sort(x,decreasing = T)[1:round(length(x)*p)]
  }else{
    sort(x,decreasing = F)[1:round(length(x)*p)]
  }
}

CalcPercGFPpos = function(x,Q=q){
  (table(x > Q)/length(x))["TRUE"]
}



###### execute ######################################
# load data
data = read.delim( file = "./FACS_data/FACS_DR.csv", sep=",",  header = T,  stringsAsFactors = F)




ID.mat <- cbind(c("NT","WT","P34G3","DR1","DR2","DR3","DR4","DR5","DR6","DR7" ,    
                  "DR8","DR9","DR10","DR11","DR12","DR13","DR14","DR15","DR16", "DR17","DR18","DR19", "DR20"),
                c("NT","WT","Reference_G3","dis_01","dis_02","dis_03","rp_01","rp_02", "pst_01","pst_02", "pst_03","pst_04",
                  "pst_05","dst_01","dst_02","dst_03","dst_04","dst_05","rst_01","rst_02","rst_03","rst_04","rst_05"),
                c("NT","WT","Reference","stem disruption","stem disruption","stem disruption","stem repair","stem repair", "posterior stem length","posterior stem length", "posterior stem length","posterior stem length",
                  "posterior stem length","distal stem length","distal stem length","distal stem length","distal stem length","distal stem length","repaired stem length","repaired stem length","repaired stem length","repaired stem length","repaired stem length"))


#Plot histograms
dada = data
colnames(dada) <- gsub("Lipo","WT",colnames(dada))
df = melt(dada)
df$Sample = sapply(as.character(df$variable) , GetTreatment)
df$Modification = sapply(  as.character(df$variable) ,  FUN=ExtractElement , y = 1 )
df$Replicate = sapply(  as.character(df$variable) ,  FUN=ExtractElement , y = 2 )
df$Modi = sapply( df$Modification , FUN = Assign_Guide_Mod, ID = ID.mat, col=2 )
df$Type = sapply( df$Modification , FUN = Assign_Guide_Mod, ID = ID.mat, col=3 )
df$GuideR = paste0(df$Modi ,"_",df$Replicate ) 
df$Type = factor(df$Type , levels = c("WT","NT","Reference", "stem disruption", "stem repair", "repaired stem length", "distal stem length"  ,  "posterior stem length"))
df$Modi = factor( df$Modi , levels = c( "WT","NT","Reference_G3" , "dis_01" , "dis_02",  "dis_03" ,
                                        "rp_02" ,  "rp_01" ,
                                        "rst_03" , "rst_05", "rst_02" , "rst_01" , "rst_04",
                                        "dst_01" , "dst_03" , "dst_05" , "dst_04" , "dst_02" ,
                                        "pst_03" ,"pst_02" , "pst_01" ,  "pst_04", "pst_05"   ))

df$GuideR = factor( df$GuideR , levels = c("WT_1","WT_2","WT_3",
                                           "NT_1","NT_2","NT_3",
                                           "Reference_G3_1", "Reference_G3_2" ,"Reference_G3_3",
                                         
                                         "dis_01_1","dis_01_2","dis_01_3",
                                         "dis_02_1","dis_02_2","dis_02_3",
                                         "dis_03_1","dis_03_2","dis_03_3",
                                         
                                         "rp_02_1","rp_02_2","rp_02_3",
                                         "rp_01_1","rp_01_2","rp_01_3",  
                                         
                                         "rst_03_1","rst_03_2","rst_03_3",      
                                         "rst_05_1","rst_05_2","rst_05_3",
                                         "rst_02_1","rst_02_2","rst_02_3",
                                         "rst_01_1","rst_01_2","rst_01_3",
                                         "rst_04_1","rst_04_2","rst_04_3",
                                         
                                         "dst_01_1","dst_01_2","dst_01_3",     
                                         "dst_03_1","dst_03_2","dst_03_3",
                                         "dst_05_1","dst_05_2","dst_05_3",  
                                         "dst_04_1","dst_04_2","dst_04_3",
                                         "dst_02_1","dst_02_2","dst_02_3",
                                         
                                         "pst_03_1","pst_03_2","pst_03_3", 
                                         "pst_02_1","pst_02_2","pst_02_3",
                                         "pst_01_1","pst_01_2","pst_01_3",
                                         "pst_04_1","pst_04_2","pst_04_3",
                                         "pst_05_1","pst_05_2","pst_05_3"))



# Plot histograms

# gg1 = ggplot(df, aes(x = value+1, y = Modi)) +
#   geom_density_ridges(aes(fill = Type)) +
#   scale_x_log10(limits=c(1,1000000)) +
#   theme_classic()+
#   ggtitle(" transfection summarized across replicates") +
#   xlab("GFP signal") +
#   ylab("DR modification") + 
#   scale_fill_manual(values = c("white","969696","#e7298a", "#db1f26", "#1a7eba","#fdc243","#7064ac","#17a04a"))
# 
# gg2 = ggplot(df, aes(x = value+1, y = GuideR)) +
#   geom_density_ridges(aes(fill = Type)) +
#   scale_x_log10(limits=c(1,1000000)) +
#   theme_classic()+
#   ggtitle(" transfection summarized across replicates") +
#   xlab("GFP signal") +
#   ylab("DR modification") + 
#   scale_fill_manual(values = c("white","969696","#e7298a", "#db1f26", "#1a7eba","#fdc243","#7064ac","#17a04a"))
# 
# pdf("./FACS_figures/DR_histograms.pdf", width = 10, height = 10, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= list(gg1,gg2) ,ncol=2))
# dev.off()


# determine how many cells are GFP positive (esp. in NT control)
q = quantile(c(data[,grep("Lipo_1",colnames(data))],data[,grep("Lipo_2",colnames(data))],data[,grep("Lipo_3",colnames(data))]), probs = c(0.99))
PercGFPpos = apply(data , 2, CalcPercGFPpos) # determine how many cells got transfected and select the same percentage across all conditions assuming equal efficiency
P = mean(PercGFPpos[grep("NT",names(PercGFPpos))])
Transfected_cells = apply(data , 2, SelectTopCells)




data = Transfected_cells
# Calculate MFI
data = cbind(data,rowMeans(data[,grep("NT",colnames(data))]))
colnames(data)[ncol(data)] = "NT"
# MFIs = colMeans(data) # colmeans to get average mfi per column
MFIs = apply(data, MARGIN = 2, Calculate_gMFI) # get geometric mean (less affected by outliers)
Reference = "NT"
output.mfi.percent  <- MFIs
for (i in 1:ncol(data)){
  output.mfi.percent[i] <- (MFIs[i]/MFIs[Reference])*100
}


# transform
df = melt(output.mfi.percent)
colnames(df)[1] <- "MFI"
df$name = names(output.mfi.percent)

toRemove = c("NT","Lipo_1","Lipo_2","Lipo_3","NT_1", "NT_2", "NT_3","WT_1","WT_2","WT_3")

if(length(which(df$name %in% toRemove)) > 0){
  del = which(df$name %in%  toRemove)
  df = df[-del,]
}



df$Modification = sapply( X = df$name ,  FUN=ExtractElement , y = 1 )
df$Replicate = sapply( X = df$name ,  FUN=ExtractElement , y = 2 )
df$Modi = sapply( df$Modification , FUN = Assign_Guide_Mod, ID = ID.mat, col=2 )
df$Type = sapply( df$Modification , FUN = Assign_Guide_Mod, ID = ID.mat, col=3 )

df$Type = factor(df$Type , levels = c("Reference", "stem disruption", "stem repair", "repaired stem length", "distal stem length"  ,  "posterior stem length"))
df$Modi = factor( df$Modi , levels = c( "Reference_G3" , "dis_01" , "dis_02",  "dis_03" ,
                                        "rp_02" ,  "rp_01" ,
                                        "rst_02" ,"rst_03" , "rst_05",  "rst_01" , "rst_04",
                                        "dst_03" , "dst_05" ,"dst_01" ,  "dst_04" , "dst_02" ,
                                        "pst_03" ,"pst_02" , "pst_04","pst_01" ,   "pst_05"   ))

pdf("./FACS_figures/FigS6c_DRmodifications.pdf", width = 6, height = 4, useDingbats = F)
ggplot( data = df, aes( x = Modi, y = MFI , color = Type )) + 
  ylab("% GFP intensity rel. to ctrl") + 
  xlab("DR-Modification") +
  geom_jitter( size = 1 , width = 0.05 , shape = 21) +
  coord_fixed(5) + #coord_fixed(7.5) +
  theme_bw() + 
  scale_color_manual(values = c("#636363","#de2d26","#3182bd","#fec44f","#756bb1", "#31a354" ))+
  guides(fill= guide_legend(title="Direct Repeat Modification"))+
  scale_y_continuous(trans='log10') +annotation_logticks(sides = "l")+
  theme(panel.grid.major.x = element_blank())+
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  stat_summary(fun.y=mean, fun.ymin=mean, fun.ymax=mean, 
               geom="crossbar", width=0.4, color="black", size = 0.15)
dev.off()




